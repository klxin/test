# 唤醒指标标注平台

## 创建分支

1. 切换到 develop 的开发分支
2. 基于 develop 分支创建新的功能分支开发

## 安装-> 启动 -> 编译

1. `npm install/yarn`安装依赖项
2. `npm start/yarn start`启动
3. `npm run build`编译

## TODO: 任务列表

1. [ ] 唤醒数据漏斗
2. [ ] 我的标注
3. [x] 随机抽样
4. [x] 按设备抽取
5. [x] 唤醒数据详情
6. [x] 疑似唤醒
7. [x] 疑似唤醒数据详情
8. [x] ASR 数据集
9. [x] ASR 数据集详情
10. [x] BadCase 数据集
11. [x] 用户管理
12. [x] 设备管理

## 接口文档地址

[唤醒标注平台接口文档](https://xiaomi.f.mioffice.cn/docs/dock4ubwFKiTcmQAz06GsMR3s1e)

## 目录约定

```bash
├─config
├─mock
├─src
│ ├─assets
│ ├─components
│ ├─constants
│ ├─layouts
│ ├─models
│ ├─pages
│ ├─services
│ ├─types
│ └─utils

```

### 目录说明

- config 存放 umi 配置文件
- mock 目录 存储 mock 文件，此目录下所有 js 和 ts 文件会被解析为 mock 文件
- /src 目录
  - pages 目录&emsp;存放所有路由组件
  - components 目录&emsp;存放公共组件
  - assets 目录&emsp;存放公共资源文件
  - layouts 目录&emsp;约定式路由时的全局布局文件
  - constants 目录&emsp;存放常量文件
  - models 目录&emsp;存放 redux 数据
  - services 目录&emsp;请求后台数据接口文件
  - types 目录&emsp;存放 typescript 定义文件
  - utils 目录&emsp;存放公共方法文件
