module.exports = {
  extends: [require.resolve('@umijs/fabric/dist/eslint')],
  globals: {
    ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION: true,
    page: true,
    REACT_APP_ENV: true,
  },
  rules: {
    'no-console': 0,
    '@typescript-eslint/no-unused-expressions': 0,
    'consistent-return': 0,
    'no-return-assign': 0,
    'jsx-a11y/media-has-caption': 0,
    '@typescript-eslint/no-shadow': 0,
    'no-return-await': 0,
    'jsx-a11y/label-has-associated-control': 0,
    'react/no-unescaped-entities': 0,
    'import/no-unresolved': 0,
  },
}
