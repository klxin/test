import { message } from 'antd'
import { saveAs } from 'filesaver.js'

/** fetch helper
 * @author {hht} hehaotian
 *
 * @param {string} url：请求链接
 */
const downloadRequest = (url, method, fileName, body) => {
  const xhr = new XMLHttpRequest()
  xhr.open(method, url, true)
  xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8')
  xhr.responseType = 'blob'
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        console.log(xhr.response, 111)
        saveAs(xhr.response, fileName)
      } else {
        message.error('下载文件失败！')
        console.log(xhr)
      }
    }
  }
  xhr.send(body)
}

export default downloadRequest
