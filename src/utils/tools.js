/**
 * @method cloneDeep 深克隆
 * @param {object} obj 接受对象
 * @return object 返回深克隆后对象
 * @desc 本方法适合简单对象的深克隆缺点如下 --> 如果有更高的需求 建议使用lodash
 * 1. 无法实现对函数、RegExp等特殊对象的克隆（会在JSON.stringify阶段被直接忽略）
 * 2. 会抛弃对象的constructor，所有构造函数会指向Object
 * 3. 对象有循环引用，会报错
 */
export const cloneDeep = obj => JSON.parse(JSON.stringify(obj))
/**
 *  @method clearAllCookie 清除所有cookie
 */
export const clearAllCookie = () => {
  var keys = document.cookie.match(/[^ =;]+(?=\=)/g)
  console.log(document.cookie, 'cookie')
  if (keys) {
    for (var i = keys.length; i--; )
      document.cookie = keys[i] + '=0;expires=' + new Date(0).toUTCString()
  }
}
/**
 * @method filterNullParams 过滤空的参数
 * @param {object} params 接受对象
 */
export const filterNullParams = (params = {}) => {
  const keys = Object.keys(params)
  const tmpObj = {}
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i]
    const val = params[key]
    if (!val) continue
    tmpObj[key] = val
  }
  return tmpObj
}
