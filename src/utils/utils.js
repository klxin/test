/**
 * @method downloadData
 * @desc   将接口的相应文件 转为Blob并下载
 * @params {object} excelUrl 下载文件地址
 * @params {object} reportName 下载文件名字
 * @params {object} config fetch默认请求对象配置
 * @params {object} ext 默认下载问文档格式
 */
export const downloadData = (
  excelUrl = '/data/download_data_set',
  reportName,
  config = {},
  ext = 'xls',
) => {
  fetch(excelUrl, {
    headers: {
      'Content-type': 'application/json;charset=UTF-8',
    },
    ...config,
  }).then((res) =>
    res.blob().then((blob) => {
      const filename = `${reportName}.${ext}`
      if (window.navigator.msSaveOrOpenBlob) {
        navigator.msSaveBlob(blob, filename) // 兼容ie10
      } else {
        const a = document.createElement('a')
        document.body.appendChild(a) // 兼容火狐，将a标签添加到body当中
        const url = window.URL.createObjectURL(blob) // 获取 blob 本地文件连接 (blob 为纯二进制对象，不能够直接保存到磁盘上)
        a.href = url
        a.download = filename
        a.target = '_blank' // a标签增加target属性
        a.click()
        a.remove() // 移除a标签
        window.URL.revokeObjectURL(url)
      }
    }),
  )
}
/* axios({
  method: 'post',
  url: 'xxxxxxx',
  data: {},
  responseType: 'blob'
}).then(res => {
   let data = res.data // 这里后端对文件流做了一层封装，将data指向res.data即可
   if (!data) {
         return
    }
    let url = window.URL.createObjectURL(new Blob([data]))
    let a = document.createElement('a')
    a.style.display = 'none'
    a.href = url
    a.setAttribute('download','excel.xls')
    document.body.appendChild(a)
    a.click() //执行下载
    window.URL.revokeObjectURL(a.href) //释放url
    document.body.removeChild(a) //释放标签
}).catch((error) => {
     console.log(error)
}) */
