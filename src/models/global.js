const GlobalModel = {
  namespace: 'global',
  state: {},
  effects: {},
  reducers: {
    changeLayoutCollapsed(
      state = {
        collapsed: true,
      },
      { payload },
    ) {
      return { ...state, collapsed: payload }
    },
  },
  subscriptions: {
    setup() {
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
    },
  },
}
export default GlobalModel
