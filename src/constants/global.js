export const SUCCESS = 'SUCCESS'
export const ERROR = 'ERROR'
export const PAGINATION = 'PAGINATION'
export const LOADING = 'LOADING'
export const ENUM_STATUS = {
  0: 'UNKNOWN', // 未知
  1: 'NOT_BEGIN', // 未开始
  2: 'LABELING', // 标注中
  3: 'COMPLETE', // 完成
  4: 'ADDING', // 添加数据中
  5: 'INVALID', // 已下线
  ADDING: 4,
  COMPLETE: 3,
  INVALID: 5,
  LABELING: 2,
  NOT_BEGIN: 1,
  UNKNOWN: 0,
}
/**
 * type_id 数据集类型
 * 0表示未知 1表示唤醒-随机抽取 2表示唤醒-按设备抽取 3表示疑似唤醒
 * 4表示ASR通用数据集 5表示ASR高频数据集 表示ASR自定义数据集
 */
// 枚举数据集类型
export const ENUM_TYPES = {
  0: 'TYPE_ALL', // 未知
  1: 'DATA_SET', // 表示唤醒-随机抽取
  2: 'DEVICE_WAKEUP', // 表示唤醒-按设备抽取
  3: 'SUSPECT_DATA_SET', // 表示疑似唤醒
  4: 'TYPE_COMMON', // 通用数据集
  5: 'TYPE_HIGHT', // 高频数据集
  6: 'TYPE_CUSTOM', // 自定义数据集
  TYPE_CUSTOM: 6,
  TYPE_HIGHT: 5,
  TYPE_COMMON: 4,
  SUSPECT_DATA_SET: 3,
  DEVICE_WAKEUP: 2,
  DATA_SET: 1,
  TYPE_ALL: 0,
}
