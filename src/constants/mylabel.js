import { FilterProcess } from '@/components/Common'

// 该页面基本和asr页面保持一致
export const ENUM_OPERATES = {
  0: 'ASR_DISPATCH', // 派发
  1: 'ASR_RECEIVE', // 领取
  2: 'ASR_OFFLINE', // 下线
  3: 'ASR_ONLINE', // 上线
  4: 'ASR_FINISH', // 完成
  5: 'ASR_ADD', // 添加
  ASR_DISPATCH: 0,
  ASR_RECEIVE: 1,
  ASR_OFFLINE: 2,
  ASR_ONLINE: 3,
  ASR_FINISH: 4,
  ASR_ADD: 5,
}
// my_label 表格columns
export const MYLABEL_COLUMNS = [
  {
    title: 'ID',
    dataIndex: 'dataset_id',
    width: 48,
  },
  {
    title: '数据集名称',
    dataIndex: 'dataset_comment',
  },
  {
    title: '数据集类型',
    dataIndex: 'dataset_type_name',
  },
  {
    title: '状态',
    dataIndex: 'dataset_status_name',
  },
  {
    title: '语料数量',
    dataIndex: 'progress',
    render: progress => progress[0] + progress[1] + progress[3],
  },
  {
    title: '未分派数量',
    dataIndex: 'not_assign',
  },
  {
    title: '标注进度',
    dataIndex: '_',
    render: (_, record) => {
      return FilterProcess(record.progress)
    },
  },
]
