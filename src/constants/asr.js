import { FilterProcess } from '@/components/Common'

export const ENUM_OPERATES = {
  0: 'ASR_DISPATCH', // 派发
  1: 'ASR_RECEIVE', // 领取
  2: 'ASR_OFFLINE', // 下线
  3: 'ASR_ONLINE', // 上线
  4: 'ASR_FINISH', // 完成
  5: 'ASR_ADD', // 添加
  ASR_DISPATCH: 0,
  ASR_RECEIVE: 1,
  ASR_OFFLINE: 2,
  ASR_ONLINE: 3,
  ASR_FINISH: 4,
  ASR_ADD: 5,
}
// asr表格columns
export const ASR_COLUMNS = [
  {
    title: 'ID',
    dataIndex: 'dataset_id',
    width: 48,
  },
  {
    title: '数据集名称',
    dataIndex: 'dataset_comment',
  },
  {
    title: '数据集类型',
    dataIndex: 'dataset_type_name',
  },
  {
    title: '状态',
    dataIndex: 'dataset_status_name',
  },
  {
    title: '语料数量',
    dataIndex: 'progress',
    render: progress => progress[0] + progress[1] + progress[3],
  },
  {
    title: '未分派数量',
    dataIndex: 'not_assign',
  },
  {
    title: '标注进度',
    dataIndex: '_',
    render: (_, record) => {
      return FilterProcess(record.progress)
    },
  },
]

// asr-desc表格columns
export const ASR_DESC_COLUMNS = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 48,
  },
  {
    title: '状态',
    dataIndex: 'status_name',
  },
  {
    title: '参考结果',
    dataIndex: 'show_result',
  },
  {
    title: '标注结果',
    dataIndex: 'label',
  },
  {
    title: 'domain',
    dataIndex: 'domain',
  },
  {
    title: '标注人',
    dataIndex: 'user_name',
  },
  {
    title: '复核人',
    dataIndex: 'check_user_name',
  },
  {
    title: '更新时间',
    dataIndex: 'update_time',
  },
]
// bad-case表格columns
export const ASR_BADCASE_COLUMNS = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 48,
  },
  {
    title: 'appId',
    dataIndex: 'app_id',
  },
  {
    title: 'query',
    dataIndex: 'query',
  },
  {
    title: 'query请求日期',
    dataIndex: 'request_time',
  },
  {
    title: '分发时PV',
    dataIndex: 'pv',
  },
  {
    title: '分发时UV',
    dataIndex: 'uv',
  },
  {
    title: '分发label',
    dataIndex: 'bad_case_label',
  },
  {
    title: '分发原因',
    dataIndex: 'bad_case_reason',
  },
  {
    title: '分发时间',
    dataIndex: 'update_time',
  },
]
