import { FilterProcess } from '@/components/Common'
/**
 *  未开始：开始标注，分派用户
 *  标注中：添加数据，完成标注，数据集下线，复核，发送邮件
 *  完成：查看相关指标，下载数据集
 *  已下线：无
 */
export const ENUM_OPERATES = {
  0: 'DATASET_ADD', // 添加数据集
  1: 'DATASET_LABEL', // 开始标注
  2: 'DATASET_DISPATCH', // 分派用户
  3: 'DATASET_ADDDATA', // 添加数据
  4: 'DATASET_COMPLETE', // 完成标注集
  5: 'DATASET_OFFLINE', // 数据集下线
  6: 'DATASET_CHECK', // 复核
  7: 'DATASET_EMAIL', // 发送邮件
  8: 'DATASET_VIEW', // 查看相关指标
  9: 'DATASET_DOWNLOAD', // 下载数据集
  DATASET_DOWNLOAD: 9,
  DATASET_VIEW: 8,
  DATASET_EMAIL: 7,
  DATASET_CHECK: 6,
  DATASET_OFFLINE: 5,
  DATASET_COMPLETE: 4,
  DATASET_ADDDATA: 3,
  DATASET_DISPATCH: 2,
  DATASET_LABEL: 1,
  DATASET_ADD: 0,
}
// 唤醒数据集页面枚举
export const ENUM_DATASET_PAGE = ['data-set', 'data-set-device-wakeup', 'data-set-suspect']

export const DATASET_COLUMNS = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 48,
  },
  {
    title: '数据集名称',
    dataIndex: 'name',
  },
  {
    title: '数据集类型',
    dataIndex: 'type_name',
  },
  {
    title: '状态',
    dataIndex: 'status_name',
  },
  {
    title: '版本号',
    dataIndex: 'version',
  },
  {
    title: '设备',
    dataIndex: 'device_name',
  },
  {
    title: '标注数量',
    dataIndex: '_',
    render: (_, record) => {
      return FilterProcess(record.progress)
    },
  },
  {
    title: '开始时间',
    dataIndex: 'device_name',
  },
  {
    title: '所属用户',
    dataIndex: 'user',
  },
]
// dataset-desc表格columns
export const DATASET_DESC_COLUMNS = [
  {
    title: 'ID',
    dataIndex: 'id',
    width: 48,
  },
  {
    title: '状态',
    dataIndex: 'status_name',
  },
  {
    title: '标注',
    dataIndex: 'show_result',
  },
  {
    title: '大模型识别',
    dataIndex: 'big_model_name',
  },
  {
    title: '唤醒厂商',
    dataIndex: 'wakeup_vendor',
  },
  {
    title: '标注人',
    dataIndex: 'user_name',
  },
  {
    title: '复核人',
    dataIndex: 'check_user_name',
  },
  {
    title: '更新时间',
    dataIndex: 'update_time',
  },
]
