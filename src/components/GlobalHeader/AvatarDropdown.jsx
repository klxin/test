import { Menu, Spin } from 'antd'
import React from 'react'
import { UserOutlined, LogoutOutlined } from '@ant-design/icons'
import { clearAllCookie } from '@/utils/tools'
import { useGetUserInfo } from '@/hooks/user'
import HeaderDropdown from '../HeaderDropdown'
import styles from './index.less'

const AvatarDropdown = () => {
  const currentUser = useGetUserInfo()
  const onMenuClick = (event) => {
    const { key } = event
    if (key === 'logout') {
      // 退出功能
      clearAllCookie()
    }
  }
  const menuHeaderDropdown = (
    <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick}>
      <Menu.Item key="logout">
        <LogoutOutlined />
        退出登录
      </Menu.Item>
    </Menu>
  )
  return currentUser && currentUser.user_name ? (
    <HeaderDropdown overlay={menuHeaderDropdown}>
      <span className={`${styles.action} ${styles.account}`}>
        <UserOutlined />
        <span className={styles.name}>{currentUser.user_name}</span>
      </span>
    </HeaderDropdown>
  ) : (
    <Spin
      size="small"
      style={{
        marginLeft: 8,
        marginRight: 8,
      }}
    />
  )
}

export default AvatarDropdown
