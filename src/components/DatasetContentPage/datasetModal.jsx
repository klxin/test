import React from 'react'
import { ENUM_STATUS } from '@/constants/global'
import { ENUM_OPERATES } from '@/constants/dataset'
import {
  AddModal,
  DispatchModal,
  AddDataModal,
  StatusModal,
  SendEmailModal,
  ViewDescModal,
} from './pageModal'

const { LABELING, COMPLETE, INVALID } = ENUM_STATUS
// 解构枚举的按钮
const {
  DATASET_ADD,
  DATASET_LABEL,
  DATASET_DISPATCH,
  DATASET_ADDDATA,
  DATASET_COMPLETE,
  DATASET_OFFLINE,
  DATASET_EMAIL,
  DATASET_VIEW,
} = ENUM_OPERATES

/**
 *
 * @param {object: {id, type, handleOk, handleCancel }} props
 * id: {number} 数据集id
 * type: {string} 表示弹框类型
 * handleOk: 确认弹框的回调函数
 * handleCancel: 取消弹框的回调函数
 */
export const DatasetModal = props => {
  const { id, type = 0, handleOk = () => {}, handleCancel = () => {}, pageName } = props
  const propsParams = {
    id,
    handleOk,
    handleCancel,
    pageName,
  }
  // 0 表示点击是添加数据集弹窗
  // 1 表示添加数据弹窗
  // 2 表示完成标注
  // 3 表示数据集下线
  // 4 表示发送邮件
  const strategyModalObj = {
    [DATASET_ADD]: <AddModal {...propsParams} />,
    [DATASET_LABEL]: <StatusModal {...propsParams} type_id={LABELING} />,
    [DATASET_DISPATCH]: <DispatchModal {...propsParams} />,
    [DATASET_ADDDATA]: <AddDataModal {...propsParams} />,
    [DATASET_COMPLETE]: <StatusModal {...propsParams} type_id={COMPLETE} />,
    [DATASET_OFFLINE]: <StatusModal {...propsParams} type_id={INVALID} />,
    [DATASET_EMAIL]: <SendEmailModal {...propsParams} />,
    [DATASET_VIEW]: <ViewDescModal {...propsParams} />,
  }
  return strategyModalObj[type] ? strategyModalObj[type] : null
}
