import {
  queryDatasetList,
  addDataset,
  batchAddLabels,
  setDatasetStatus,
  sendCheckMail,
  assignDatasetUser,
  batchAddDeviceLabels,
} from '@/services/dataset'
import { ENUM_DATASET_PAGE } from '@/constants/dataset'

const initConfig = {
  tableRequest: queryDatasetList,
  AddModalRequest: addDataset,
  StatusModalRequest: setDatasetStatus,
  AddDataModalRequest: batchAddLabels,
  DispatchModalRequest: assignDatasetUser,
  SendEmailModalRequest: sendCheckMail,
}
export const strategyPageApi = (pageName = 'data-set') => {
  const pageMapRequest = {
    [ENUM_DATASET_PAGE[0]]: initConfig,
    [ENUM_DATASET_PAGE[1]]: { ...initConfig, AddDataModalRequest: batchAddDeviceLabels },
    [ENUM_DATASET_PAGE[2]]: { ...initConfig },
  }
  return pageMapRequest[pageName]
}
