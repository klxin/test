import React, { useEffect } from 'react'
import { Modal, Form, InputNumber, Select, Button, Space, Divider, message, Input } from 'antd'
import { useDeviceInfo } from '@/hooks/device'
import { useQueryLabelUsers } from '@/hooks/user'
import { useGetDatasetResult, useGetDataSetSuspectResult } from '@/hooks/dataset'
import { ENUM_STATUS } from '@/constants/global'
import { ENUM_DATASET_PAGE } from '@/constants/dataset'
import { strategyPageApi } from './strategyPageApi'
import './index.less'

const { LABELING, COMPLETE, INVALID } = ENUM_STATUS
const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
}
// 添加数据集弹框
export const AddModal = (props) => {
  const { handleOk, handleCancel, pageName } = props
  const [form] = Form.useForm()
  const device_options = useDeviceInfo()
  useEffect(() => {
    form.setFieldsValue({
      device: device_options[0] && device_options[0].id,
    })
  }, [device_options, form])
  const onFinish = async (values) => {
    try {
      const result = await strategyPageApi(pageName).AddModalRequest({ type: 1, ...values })
      if (result.code !== 0) return
      message.success(result.desc)
      form.resetFields()
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }
  return (
    <Modal
      title="请确认信息"
      visible
      onCancel={handleCancel}
      footer={null}
      className="dataset-modal"
    >
      <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
        <Form.Item label="选择设备" name="device" rules={[{ required: true }]}>
          <Select placeholder="请选择设备" allowClear>
            {device_options.map((item) => (
              <Select.Option key={item.id} value={item.id}>
                {item.id}-{item.comment}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item label="版本号" name="version">
          <Input placeholder="填写数据集的版本号，不填表示所有版本" />
        </Form.Item>
        <Divider />
        <div className="modal-footer mt10">
          <Space>
            <Button type="primary" htmlType="submit">
              确认
            </Button>
            <Button htmlType="button" onClick={handleCancel}>
              取消
            </Button>
          </Space>
        </div>
      </Form>
    </Modal>
  )
}
// 更新状态 弹框
export const StatusModal = (props) => {
  const { id, type_id, handleOk, handleCancel, pageName } = props
  const handleOk1 = async () => {
    try {
      const result = await strategyPageApi(pageName).StatusModalRequest({
        data_set_id: id,
        type_id,
      })
      if (result.code !== 0) return
      message.success(result.desc)
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }
  const strategyContent = {
    [COMPLETE]: <p>确认结束标注吗？结束标注将开始自动计算这个数据集的相关指标</p>,
    [INVALID]: <p>确认下线数据集吗？</p>,
    [LABELING]: <p>确认开始标注吗？</p>,
  }
  return (
    <Modal title="请确认信息" visible onOk={handleOk1} onCancel={handleCancel}>
      {strategyContent[type_id]}
    </Modal>
  )
}
// 添加数据弹框
export const AddDataModal = (props) => {
  const { id: data_set_id, handleOk, handleCancel, pageName } = props
  const [form] = Form.useForm()
  const onFinish = async (values) => {
    try {
      const result = await strategyPageApi(pageName).AddDataModalRequest({ data_set_id, ...values })
      if (result.code !== 0) return
      message.success(result.desc)
      form.resetFields()
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }
  return (
    <Modal
      title="将为数据集 小米10高通引擎_MIUIV12.2.2.0.RJBCNXM版本 全版本添加数据"
      visible
      onCancel={handleCancel}
      footer={null}
      className="dataset-modal"
    >
      <Form form={form} name="control-hooks" initialValues={{ num: 100 }} onFinish={onFinish}>
        <Form.Item
          label="生成数据（只有该数据集的负责人可以操作）"
          name="num"
          rules={[{ required: true }]}
        >
          <InputNumber placeholder="要新生成多少条数据，填写10-1000的整数" />
        </Form.Item>
        <div className="modal-footer mt10">
          <Space>
            <Button type="primary" htmlType="submit">
              确认
            </Button>
            <Button htmlType="button" onClick={handleCancel}>
              取消
            </Button>
          </Space>
        </div>
      </Form>
    </Modal>
  )
}
// 分派用户弹框
export const DispatchModal = (props) => {
  const { id: data_set_id, handleOk, handleCancel, pageName } = props
  const [form] = Form.useForm()
  const userList = useQueryLabelUsers()
  const onFinish = async (values) => {
    try {
      const result = await strategyPageApi(pageName).DispatchModalRequest({
        data_set_id,
        ...values,
      })
      if (result.code !== 0) return
      message.success(result.desc)
      form.resetFields()
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }
  return (
    <Modal
      title="请确认分派用户信息"
      visible
      onCancel={handleCancel}
      footer={null}
      className="dataset-modal"
    >
      <Form form={form} name="control-hooks" initialValues={{ num: 100 }} onFinish={onFinish}>
        <Form.Item label="" name="user_id" rules={[{ required: true }]}>
          <Select placeholder="请选择要分派的用户" allowClear>
            {userList.map((item) => (
              <Select.Option key={item.id} value={item.id}>
                {item.id}-{item.name}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <div className="modal-footer mt10">
          <Space>
            <Button type="primary" htmlType="submit">
              确认
            </Button>
            <Button htmlType="button" onClick={handleCancel}>
              取消
            </Button>
          </Space>
        </div>
      </Form>
    </Modal>
  )
}
// 发送邮件弹框
export const SendEmailModal = (props) => {
  const { id, handleOk, handleCancel, pageName } = props
  const handleOk1 = async () => {
    try {
      const result = await strategyPageApi(pageName).SendEmailModalRequest({
        data_set_id: id,
        type_id: 1,
      })
      if (result.code !== 0) return
      message.success(result.desc)
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }
  return (
    <Modal title="确认发送复核邮件吗？" visible onOk={handleOk1} onCancel={handleCancel}>
      <p>确认发送复核邮件吗？</p>
    </Modal>
  )
}
// 查看相关指标
export const ViewDescModal = (props) => {
  const { id: data_set_id, handleCancel, pageName } = props
  const pageMapData = {
    [ENUM_DATASET_PAGE[0]]: useGetDatasetResult,
    [ENUM_DATASET_PAGE[1]]: useGetDatasetResult,
    [ENUM_DATASET_PAGE[2]]: useGetDataSetSuspectResult,
  }
  const descInfo = pageMapData[pageName](data_set_id)
  const showDataByPageName = {
    [ENUM_DATASET_PAGE[0]]: (
      <>
        <div className="list">
          <h2>误唤醒率:{descInfo['42']}</h2>
          <Divider />
          <p>没有唤醒词：{descInfo['27']}</p>
          <p>无意中提到唤醒词：{descInfo['28']}</p>
          <p>汇总：{descInfo.all}</p>
          <p>设备日均误唤醒次数：{descInfo.device_average_wu}</p>
        </div>
        <div className="list">
          <h2>唤醒统计:{descInfo['42']}</h2>
          <Divider />
          <p>TP：{descInfo['31']}</p>
          <p>FP：{descInfo['32']}</p>
          <p>TN：{descInfo['33']}</p>
          <p>FN：{descInfo['34']}</p>
          <p>Precision：{descInfo.precision}</p>
          <p>Recall：{descInfo.recall}</p>
        </div>
      </>
    ),
    [ENUM_DATASET_PAGE[1]]: (
      <>
        <div className="list">
          <h2>误唤醒统计</h2>
          <Divider />
          <p>误唤醒率{descInfo['42']}</p>
          <p>误唤醒设备占比{descInfo.unwake_device_percent}</p>
        </div>
        <div className="list">
          <h2>误唤醒次数设备数分布</h2>
          <Divider />
          <p>
            误唤醒 0 次设备数：{descInfo['44']} 占比：{descInfo['0_percent']}
          </p>
          <p>
            误唤醒 1 次设备数：{descInfo['45']} 占比：{descInfo['1_percent']}
          </p>
          <p>
            误唤醒 2 次设备数：{descInfo['46']} 占比：{descInfo['2_percent']}
          </p>
          <p>
            误唤醒 2 次以上设备数：{descInfo['47']} 占比：{descInfo['3_percent']}
          </p>
        </div>
      </>
    ),
    [ENUM_DATASET_PAGE[2]]: (
      <>
        <div className="list">
          <h2>有效数据占比：</h2>
          <Divider />
          <p>误唤醒率{descInfo['4']}</p>
          <p>汇总：{descInfo.unwake_device_percent}</p>
        </div>
        <div className="list">
          <h2>唤醒统计：</h2>
          <Divider />
          <p>只有唤醒词：{descInfo['48']}</p>
          <p>唤醒词+指令：{descInfo['49']}</p>
          <p>无意提到唤醒词：{descInfo['50']}</p>
          <p>没有唤醒词：{descInfo['51']}</p>
          <p>无意义：{descInfo['52']}</p>
        </div>
      </>
    ),
  }
  return (
    <Modal title="唤醒数据指标如下：" visible onCancel={handleCancel} footer={null}>
      {showDataByPageName[pageName]}
      <div className="dataset-modal-footer">
        <Button type="primary" danger onClick={handleCancel}>
          关闭
        </Button>
      </div>
    </Modal>
  )
}
