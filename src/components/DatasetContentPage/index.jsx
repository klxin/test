import React, { useRef, useState } from 'react'
import { Link } from 'umi'
import { Card, Space, Divider } from 'antd'
import ProTable from '@ant-design/pro-table'
import { ENUM_OPERATES, DATASET_COLUMNS, ENUM_DATASET_PAGE } from '@/constants/dataset'
import { ENUM_TYPES, ENUM_STATUS } from '@/constants/global'
import { DatasetModal } from './datasetModal'
import { strategyPageApi } from './strategyPageApi'
// import { downloadData } from '@/utils/utils'
// import downloadRequest from '@/utils/downloadRequest'

const { NOT_BEGIN, LABELING, COMPLETE, INVALID } = ENUM_STATUS
const {
  DATASET_ADD,
  DATASET_LABEL,
  DATASET_DISPATCH,
  DATASET_ADDDATA,
  DATASET_COMPLETE,
  DATASET_OFFLINE,
  DATASET_CHECK,
  DATASET_EMAIL,
  DATASET_VIEW,
  DATASET_DOWNLOAD,
} = ENUM_OPERATES
// 表格中的按钮配置列表
const btnConfigList = [
  { name: '添加数据集', value: DATASET_ADD, className: 'btn-green' },
  { name: '开始标注', value: DATASET_LABEL, className: 'btn-primary' },
  { name: '分派用户', value: DATASET_DISPATCH, className: 'btn-orange' },
  { name: '添加数据', value: DATASET_ADDDATA, className: 'btn-green' },
  { name: '完成标注', value: DATASET_COMPLETE, className: 'btn-red' },
  { name: '数据集下线', value: DATASET_OFFLINE, className: 'btn-gray' },
  { name: '复核', value: DATASET_CHECK, className: 'btn-orange' },
  { name: '发送邮件', value: DATASET_EMAIL, className: 'btn-purple' },
  { name: '查看相关指标', value: DATASET_VIEW, className: 'btn-primary' },
  { name: '下载数据集', value: DATASET_DOWNLOAD, className: 'btn-primary' },
]
const pageMapTypeId = {
  [ENUM_DATASET_PAGE[0]]: ENUM_TYPES.DATA_SET,
  [ENUM_DATASET_PAGE[1]]: ENUM_TYPES.DEVICE_WAKEUP,
  [ENUM_DATASET_PAGE[2]]: ENUM_TYPES.SUSPECT_DATA_SET,
}
/**
 * {object} props 参数如下
 *  props.columns    默认为DATASET_COLUMNS 如果传递需要为数组 满足Table组件的columns要求
 *  props.render     默认为null 用于渲染自定义的表格查询头部或其他内容展示
 *  props.checkUrl   默认为数据集详情 表示复核的页面地址
 *  props.pageName   默认为 data-set 表示页面名字 不同页面请求接口不同
 */
export default (props) => {
  const {
    columns = DATASET_COLUMNS,
    checkUrl = '/wakeup-datas/data-set-desc',
    pageName = 'data-set',
  } = props
  const actionRef = useRef()
  const [params, setParams] = useState({ type_id: pageMapTypeId[pageName] })
  const [datasetId, setDatasetId] = useState(0)
  const [visible, setVisible] = useState(false)
  const [modalType, setModalType] = useState(0)
  const onFinish = (values) => {
    setParams(values)
  }
  const onAddCallback = (type = DATASET_ADD) => {
    setVisible(true)
    setModalType(type)
  }
  const handleOperate = async (datasetId, type) => {
    setDatasetId(datasetId)
    setModalType(type)
    if (type !== DATASET_DOWNLOAD) return setVisible(true)
  }
  // 重置弹框事件
  const handleResetModal = () => {
    setVisible(false)
    setModalType('')
  }
  const handleOk = () => {
    // 重置弹框默认值
    handleResetModal()
    actionRef.current.reload()
  }
  const handleCancel = () => {
    // 重置弹框默认值
    handleResetModal()
  }
  const operationRender = (_, { id: data_set_id, status }) => {
    const linkUrl = `${checkUrl}?data_set_id=${data_set_id}&type_id=${params.type_id}&status=${status}`
    const downloadUrl = `/api/data/download_data_set?data_set_id=${data_set_id}`
    // dataset_status数值对应的含义 查看@/constants/asr 中的ENUM_STATUS
    const btnList = btnConfigList.map((item) => {
      if (item.value === DATASET_DOWNLOAD)
        return (
          <a className={item.className} download href={downloadUrl}>
            {item.name}
          </a>
        )
      return (
        <span className={item.className} onClick={() => handleOperate(data_set_id, item.value)}>
          {item.name}
        </span>
      )
    })
    const strategyStatue = {
      [NOT_BEGIN]: (
        <>
          {btnList[1]}
          {btnList[2]}
        </>
      ),
      [LABELING]: (
        <>
          {btnList[3]}
          {btnList[4]}
          {btnList[5]}
          {/* {btnList[6]} */}
          <Link className="btn-orange" to={linkUrl}>
            复核
          </Link>
          {btnList[9]}
          {btnList[7]}
        </>
      ),
      [COMPLETE]: (
        <>
          {btnList[8]}
          {btnList[9]}
        </>
      ),
      [INVALID]: (
        <>
          <Link className="btn-orange" to={linkUrl}>
            复核
          </Link>
          {btnList[9]}
        </>
      ),
    }
    return <Space>{strategyStatue[status]}</Space>
  }
  const COLUMNS = [
    ...columns,
    {
      title: '查看',
      dataIndex: '_',
      render: (_, { id, status }) => {
        const url = `${checkUrl}?data_set_id=${id}&type_id=${params.type_id}`
        return status === LABELING ? (
          <Link to={url}>查看详情</Link>
        ) : (
          <span className="gray">查看详情</span>
        )
      },
    },
    {
      title: '操作',
      dataIndex: '_',
      render: operationRender,
    },
  ]
  return (
    <Card>
      {/* 表格查询组件头部 */}
      {/* <TableQueryHead onFinish={onFinish} onAddCallback={onAddCallback} /> */}
      {props.render ? props.render({ onFinish, onAddCallback }) : null}
      <Divider />
      <ProTable
        columns={COLUMNS}
        actionRef={actionRef}
        request={async (params = {}) => strategyPageApi(pageName).tableRequest(params)}
        rowKey="id"
        search={false}
        params={params}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
      />
      ){/* 弹框 */}
      {visible && (
        <DatasetModal
          id={datasetId}
          type={modalType}
          handleOk={handleOk}
          handleCancel={handleCancel}
          pageName={pageName}
        />
      )}
    </Card>
  )
}
