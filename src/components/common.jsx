export const FilterProcess = (progress = []) => (
  <>
    <span className="font-red"> {progress[0]} </span>/
    <span className="font-green"> {progress[1]} </span>/
    <span className="font-primary"> {progress[3]} </span>/
    <span className="font-gray">
      {progress[0] + progress[1] + progress[3]}({progress[4]})
    </span>
  </>
)
// status=0只有标注按钮
// status=1只有标注按钮
// status=2已下线，只有标注按钮
// status=3待复核，有复核和标注按钮
//btns=[标注按钮, 复核按钮]
export const statusMapBtn = (status = 0, btns = []) => {
  if (status === 0 || status === 1 || status === 2) return btns[0]
  if (status === 3)
    return (
      <>
        {btns[0]}
        {btns[1]}
      </>
    )
  return null
}
