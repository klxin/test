import { useReducer } from 'react'
import { SUCCESS, ERROR, PAGINATION, LOADING } from '@/constants/global'

const reducer = (state = {}, action = {}) => {
  const { type, payload } = action
  switch (type) {
    case SUCCESS:
      return {
        ...state,
        loading: false,
        data: payload.data,
        pagination: { ...state.pagination, ...payload.pagination },
      }
    case ERROR:
      return { ...state, loading: false, data: [] }
    case PAGINATION:
      return { ...state, pagination: { ...state.pagination, ...payload } }
    case LOADING:
      return { ...state, loading: payload }
    default:
      return state
  }
}

export function useTablePro() {
  const initialState = {
    data: [],
    loading: true,
    pagination: {
      position: ['bottomRight'],
      current: 1,
      pageSize: 10,
      pageSizeOptions: ['10', '20', '40', '80'],
      total: 0,
      showTotal: total => `共计${total}条数据 `,
      showSizeChanger: true,
      onChange: onPageChange,
      onShowSizeChange: onPageSizeChange,
    },
  }
  const [state, dispatch] = useReducer(reducer, initialState)

  function onPageChange(current) {
    dispatch({ type: PAGINATION, payload: { current } })
  }
  function onPageSizeChange(pageSize) {
    dispatch({ type: PAGINATION, payload: { pageSize } })
  }
  return { state, dispatch }
}
