import React from 'react'
import { Table } from 'antd'
import { PAGINATION } from '@/constants/global'
import { useTablePro } from './useTablePro'

export function TablePro(props) {
  const {
    columns = {},
    dataSource = [],
    loading = true,
    rowKey = 'id',
    rowSelection,
    expandable,
    title,
    onCallback,
    showPagination = true,
  } = props
  const { state, dispatch } = useTablePro()
  function onChange(page) {
    dispatch({ type: PAGINATION, payload: page })
    onCallback && onCallback()
  }
  return (
    <Table
      columns={columns}
      dataSource={dataSource}
      rowKey={rowKey}
      pagination={!showPagination ? false : { ...state.pagination }}
      rowSelection={rowSelection}
      expandable={expandable}
      loading={loading}
      title={title}
      onChange={onChange}
    />
  )
}
export default useTablePro
