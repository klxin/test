import React, { useEffect, useState } from 'react'
import {
  Modal,
  Form,
  InputNumber,
  Select,
  Button,
  Space,
  Card,
  Divider,
  message,
  Radio,
  Popover,
} from 'antd'
import { useQueryAssignInfo, useQueryLabelReceive } from '@/hooks/asr'
import { useQueryLabelUsers } from '@/hooks/user'
import { useDeviceInfo } from '@/hooks/device'
import { assignLabel, addAsrDataSet } from '@/services/asr'
import { setStatus } from '@/services/global'
import { ENUM_STATUS, ENUM_TYPES } from '@/constants/global'
import './index.less'

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
}
// TODO: 合并派发和领取弹框
// 派发弹框
export const DispatchModal = (props) => {
  const { id, handleOk, handleCancel } = props
  const [form] = Form.useForm()
  const labelUsers = useQueryLabelUsers()
  const assignInfo = useQueryAssignInfo(id)
  useEffect(() => {
    form.setFieldsValue({
      user_id: labelUsers[0] && labelUsers[0].id,
    })
  }, [form, id, labelUsers])

  const handleCancel1 = () => {
    // 重置表单
    form.resetFields()
    handleCancel && handleCancel()
  }
  const onFinish = async (values) => {
    try {
      const result = await assignLabel({ ...values, id })
      if (result.code !== 0) return
      message.success(result.desc)
      form.resetFields()
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <Modal title="请确认信息" visible width={800} onCancel={handleCancel1} footer={null}>
      <Card style={{ backgroundColor: '#fdf3d8', marginBottom: 30 }}>
        {assignInfo.map((item) => (
          <p key={item.id}>
            {item.name} 已领取： {item.num} 条
          </p>
        ))}
      </Card>
      <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
        <Form.Item name="user_id" label="分配给谁" rules={[{ required: true }]}>
          <Select style={{ width: '80%' }}>
            {labelUsers &&
              labelUsers.map((item) => (
                <Select.Option key={item.id} value={item.id}>
                  {item.name}
                </Select.Option>
              ))}
          </Select>
        </Form.Item>
        <Form.Item name="num" label="分配数量" rules={[{ required: true }]}>
          <InputNumber placeholder="请输入0和未分派数值之间的整数" style={{ width: '80%' }} />
        </Form.Item>
        <Divider />
        <div className="modal-footer mt10">
          <Space>
            <Button type="primary" htmlType="submit">
              确认
            </Button>
            <Button htmlType="button" onClick={handleCancel}>
              取消
            </Button>
          </Space>
        </div>
      </Form>
    </Modal>
  )
}
// 领取弹框
export const ReceiveModal = (props) => {
  const { id, handleOk, handleCancel } = props
  const [form] = Form.useForm()
  const labelReceives = useQueryLabelReceive()
  const handleCancel1 = () => {
    // 重置表单
    form.resetFields()
    handleCancel && handleCancel()
  }
  const onFinish = async (values) => {
    try {
      const result = await assignLabel({ ...values, id })
      if (result.code !== 0) return
      message.success(result.desc)
      form.resetFields()
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <Modal title="请确认信息" visible width={800} onCancel={handleCancel} footer={null}>
      <Card style={{ backgroundColor: '#fdf3d8', marginBottom: 30 }}>
        <p>您已经领取了:{labelReceives && labelReceives[0]}条</p>
        <p>本数据集未分派数量: {labelReceives && labelReceives[1]}条</p>
      </Card>
      <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
        <Form.Item name="num" label="请输入要领取的数量" rules={[{ required: true }]}>
          <InputNumber placeholder="请输入0和未分派数值之间的整数" style={{ width: '80%' }} />
        </Form.Item>
        <Divider />
        <div className="modal-footer mt10">
          <Space>
            <Button type="primary" htmlType="submit">
              确认
            </Button>
            <Button htmlType="button" onClick={handleCancel1}>
              取消
            </Button>
          </Space>
        </div>
      </Form>
    </Modal>
  )
}
// 下线弹框
export const OfflineModal = (props) => {
  const { id, handleOk, handleCancel } = props
  const handleOk1 = async () => {
    try {
      const result = await setStatus({ data_set_id: id, type_id: ENUM_STATUS.INVALID })
      if (result.code !== 0) return
      message.success(result.desc)
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }
  return (
    <Modal title="请确认信息" visible onOk={handleOk1} onCancel={handleCancel}>
      <p>确认下线数据吗？</p>
    </Modal>
  )
}
// 上线弹框
export const OnlineModal = (props) => {
  const { id, handleOk, handleCancel } = props
  const handleOk1 = async () => {
    try {
      const result = await setStatus({ data_set_id: id, type_id: ENUM_STATUS.LABELING })
      if (result.code !== 0) return
      message.success(result.desc)
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }
  return (
    <Modal title="请确认信息" visible onOk={handleOk1} onCancel={handleCancel}>
      <p>确认下线数据吗？</p>
    </Modal>
  )
}
// 完成弹框
export const FinishModal = (props) => {
  const { id, handleOk, handleCancel } = props
  const handleOk1 = async () => {
    try {
      const result = await setStatus({ data_set_id: id, type_id: ENUM_STATUS.COMPLETE })
      if (result.code !== 0) return
      message.success(result.desc)
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }
  return (
    <Modal title="请确认信息" visible onOk={handleOk1} onCancel={handleCancel}>
      <h1>确认完成标注并计算指标吗？</h1>
      <h1 className="font-red">注意：此过程将不可逆!</h1>
    </Modal>
  )
}
// 添加数据集弹框
export const AddModal = (props) => {
  const { handleOk, handleCancel } = props
  const [form] = Form.useForm()
  const [addParams, setAddParams] = useState({ cnum: 500, topn: 100, tnum: 100, file_name: '' })
  const device_options = useDeviceInfo()
  useEffect(() => {
    form.setFieldsValue({
      device_id: device_options[0] && device_options[0].id,
    })
  }, [device_options, form])
  const hanldeChangeParams = (key, val) => {
    setAddParams({ ...addParams, [key]: val })
  }
  const onFinish = async (values) => {
    try {
      const result = await addAsrDataSet({ ...values, ...addParams })
      console.log(result)
      if (result.code !== 0) return
      message.success(result.desc)
      form.resetFields()
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }

    // handleOk && handleOk()
  }
  return (
    <Modal title="请确认信息" visible onCancel={handleCancel} footer={null} className="asr-modal">
      <Form
        {...layout}
        form={form}
        name="control-hooks"
        initialValues={{
          type_id: ENUM_TYPES.TYPE_COMMON,
        }}
        onFinish={onFinish}
      >
        <Form.Item label="选择设备" name="device_id" rules={[{ required: true }]}>
          <Select placeholder="请选择设备" allowClear>
            {device_options.map((item) => (
              <Select.Option key={item.id} value={item.id}>
                {item.id}-{item.comment}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item name="type_id" label="数据集类型" rules={[{ required: true }]}>
          <Radio.Group>
            <div className="radio-item">
              <Radio value={ENUM_TYPES.TYPE_COMMON}>
                通用数据集类型
                <div className="radio-item-list  mt10">
                  <span>生成数量(请输入100-1000的整数):</span>
                  <InputNumber
                    name="cnum"
                    min="100"
                    max="100"
                    defaultValue="500"
                    onChange={(value) => hanldeChangeParams('cnum', value)}
                  />
                </div>
              </Radio>
            </div>
            <div className="radio-item">
              <Radio value={ENUM_TYPES.TYPE_HIGHT}>
                高频数据集
                <div className="radio-item-list mt10">
                  <span>输入要生成前N名query的N值:</span>
                  <InputNumber name="topn" defaultValue="100" />
                </div>
                <div className="radio-item-list  mt10">
                  <span>输入每条query生成的数量:</span>
                  <InputNumber name="tnum" defaultValue="10" />
                </div>
              </Radio>
            </div>
            <div className="radio-item">
              <Popover content="自定义数据集功能未开放，敬请期待！" title="Title" trigger="hover">
                <Radio value={ENUM_TYPES.TYPE_CUSTOM} disabled>
                  自定义数据集
                  <div className="radio-item-list  mt10">
                    <label>
                      <span className="btn-green">上传</span>
                      <input disabled type="file" name="file_name" style={{ display: 'none' }} />
                    </label>
                    {/* <a href="javascript:;">下载模板</a> */}
                    <span>下载模板</span>
                  </div>
                </Radio>
              </Popover>
            </div>
          </Radio.Group>
        </Form.Item>
        <Divider />
        <div className="modal-footer mt10">
          <Space>
            <Button type="primary" htmlType="submit">
              确认
            </Button>
            <Button htmlType="button" onClick={handleCancel}>
              取消
            </Button>
          </Space>
        </div>
      </Form>
    </Modal>
  )
}
