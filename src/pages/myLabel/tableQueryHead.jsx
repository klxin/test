import React from 'react'
import { Form, Button, Select } from 'antd'
import { useDeviceInfo } from '@/hooks/device'
import { useGetDatasetStatusList, useGetDatasetTypeList } from '@/hooks/enum'

export default (props) => {
  const { onFinish = () => {}, onAddCallback = () => {} } = props
  const [form] = Form.useForm()
  const device_options = useDeviceInfo()
  const statusList = useGetDatasetStatusList()
  const typeList = useGetDatasetTypeList()
  return (
    <Form layout="inline" form={form} onFinish={onFinish}>
      <Form.Item label="选择设备" name="device_id">
        <Select placeholder="请选择设备" allowClear>
          <Select.Option key="0" value="0">
            0-全部
          </Select.Option>
          {device_options.map((item) => (
            <Select.Option key={item.id} value={item.id}>
              {item.id}-{item.comment}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item label="数据集状态" name="status">
        <Select placeholder="请选择数据集状态" allowClear>
          {statusList.map((item) => (
            <Select.Option key={item.status_id} value={item.status_id}>
              {item.status_name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item label="数据集类型" name="type_id">
        <Select placeholder="请选择数据集类型" allowClear>
          {typeList.map((item) => (
            <Select.Option key={item.type_id} value={item.type_id}>
              {item.type_name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          查询
        </Button>
      </Form.Item>
      <Form.Item>
        <span className="btn-green" onClick={() => onAddCallback()}>
          添加数据集
        </span>
      </Form.Item>
    </Form>
  )
}
