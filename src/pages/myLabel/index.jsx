import React, { useRef, useState } from 'react'
import { PageHeaderWrapper } from '@ant-design/pro-layout'
import { Button, Space, Divider } from 'antd'
import ProTable from '@ant-design/pro-table'
import { queryMyLabel } from '@/services/mylabel'
import { ENUM_STATUS } from '@/constants/global'
import { MYLABEL_COLUMNS, ENUM_OPERATES } from '@/constants/myLabel'
import TableQueryHead from './tableQueryHead'
import { MyLabelModal } from './myLabelModal'

export default (props) => {
  const actionRef = useRef()
  const [params, setParams] = useState({})
  const [datasetId, setDatasetId] = useState(0)
  const [visible, setVisible] = useState(false)
  const [modalType, setModalType] = useState('')
  const onFinish = (values) => {
    setParams(values)
  }
  const onAddCallback = (type = ENUM_OPERATES[5]) => {
    setVisible(true)
    setModalType(type)
  }
  const handleOperate = (datasetId, type) => {
    setDatasetId(datasetId)
    setVisible(true)
    setModalType(type)
  }
  // 重置弹框事件
  const handleResetModal = () => {
    setVisible(false)
    setModalType('')
  }
  const handleOk = () => {
    // 重置弹框默认值
    handleResetModal()
    actionRef.current.reload()
  }
  const handleCancel = () => {
    // 重置弹框默认值
    handleResetModal()
  }
  const COLUMNS = [
    ...MYLABEL_COLUMNS,
    {
      title: '操作',
      dataIndex: 'data_set_id',
      render: (data_set_id, { dataset_status }) => {
        const linkUrl = `/ASR-datas/asr-desc?data_set_id=${data_set_id}&status=${dataset_status}`
        // dataset_status数值对应的含义 查看@/constants/asr 中的ENUM_STATUS
        const strategyBtn = {
          [ENUM_STATUS.LABELING]: (
            <>
              <Button type="primary" onClick={() => handleOperate(data_set_id, ENUM_OPERATES[0])}>
                分派
              </Button>
              <Button type="primary" onClick={() => handleOperate(data_set_id, ENUM_OPERATES[1])}>
                领取
              </Button>
              <Button type="primary" onClick={() => handleOperate(data_set_id, ENUM_OPERATES[2])}>
                下线
              </Button>
              <span className="btn-orange" onClick={() => props.history.push(linkUrl)}>
                复核
              </span>
              <Button
                type="primary"
                danger
                onClick={() => handleOperate(data_set_id, ENUM_OPERATES[4])}
              >
                完成
              </Button>
            </>
          ),
          [ENUM_STATUS.COMPLETE]: (
            <Button type="primary" onClick={() => handleOperate(data_set_id, ENUM_OPERATES[2])}>
              下线
            </Button>
          ),
          [ENUM_STATUS.INVALID]: (
            <Button type="primary" onClick={() => handleOperate(data_set_id, ENUM_OPERATES[3])}>
              上线
            </Button>
          ),
          [ENUM_STATUS.ADDING]: null,
        }
        return <Space>{strategyBtn[dataset_status]}</Space>
      },
    },
    {
      title: '相关指标',
      dataIndex: '',
      width: 50,
    },
  ]
  return (
    <PageHeaderWrapper>
      {/* 表格查询组件头部 */}
      <TableQueryHead onFinish={onFinish} onAddCallback={onAddCallback} />
      <Divider />
      {/* <Link to="/ASR-datas/asr-desc">去ASR数据集页面详情</Link> */}
      <ProTable
        columns={COLUMNS}
        actionRef={actionRef}
        request={async (params = {}) => queryMyLabel(params)}
        rowKey="data_set_id"
        search={false}
        params={params}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
      />
      {visible && (
        <MyLabelModal
          id={datasetId}
          type={modalType}
          handleOk={handleOk}
          handleCancel={handleCancel}
        />
      )}
    </PageHeaderWrapper>
  )
}
