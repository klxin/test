import React from 'react'
import { ENUM_OPERATES } from '@/constants/asr'
import { DispatchModal, ReceiveModal, OfflineModal, FinishModal, AddModal } from './pageModal'
/**
 *
 * @param {object: {id, type, handleOk, handleCancel }} props
 * id: {number} 数据集id
 * type: {string} 表示弹框类型
 * handleOk: 确认弹框的回调函数
 * handleCancel: 取消弹框的回调函数
 */
export const MyLabelModal = props => {
  const { id, type = ENUM_OPERATES[0], handleOk = () => {}, handleCancel = () => {} } = props
  const propsParams = {
    id,
    handleOk,
    handleCancel,
  }
  const strategyModalObj = {
    [ENUM_OPERATES[0]]: <DispatchModal {...propsParams} />,
    [ENUM_OPERATES[1]]: <ReceiveModal {...propsParams} />,
    [ENUM_OPERATES[2]]: <OfflineModal {...propsParams} />,
    [ENUM_OPERATES[3]]: <OfflineModal {...propsParams} />,
    [ENUM_OPERATES[4]]: <FinishModal {...propsParams} />,
    [ENUM_OPERATES[5]]: <AddModal {...propsParams} />,
  }
  return strategyModalObj[type] ? strategyModalObj[type] : null
}
