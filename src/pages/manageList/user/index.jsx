import React, { useEffect } from 'react'
import { Card } from 'antd'
import { queryUserList } from '@/services/user'
import useTablePro, { TablePro } from '@/components/TablePro'
import { SUCCESS, ERROR } from '@/constants/global'
import { columns } from './columns'

export default () => {
  const { state, dispatch } = useTablePro()
  useEffect(() => {
    queryUserList()
      .then((res) => {
        if (res.code !== 0) dispatch({ type: ERROR })
        dispatch({
          type: SUCCESS,
          payload: { data: res.data, pagination: { total: res.data.length } },
        })
      })
      .catch((err) => {
        console.error(err)
        dispatch({ type: ERROR })
      })
  }, [dispatch])
  return (
    <Card>
      <TablePro
        columns={columns}
        dataSource={state.data}
        pagination={false}
        loading={state.loading}
      />
    </Card>
  )
}
