export const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: '用户名称',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: '用户中文名',
    dataIndex: 'c_name',
    key: 'c_name',
  },
]
