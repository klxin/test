import React, { useEffect } from 'react'
import { Modal, Form, Input, Radio, Checkbox, Row, Col, Button, message, Space } from 'antd'
import { addDevice, editDevice } from '@/services/device'
const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
}
export const DeviceModal = props => {
  const { visible = true, closeModal, data = {}, handleQueryList } = props
  const [form] = Form.useForm()
  useEffect(() => {
    form.setFieldsValue(data)
  }, [data, form])

  const handleCancel = () => {
    // 重置表单
    form.resetFields()
    closeModal && closeModal()
  }
  const onFinish = async values => {
    const { checkboxGroup, ...params } = values
    params.has_wakeup = checkboxGroup.includes('has_wakeup') ? 1 : 2
    params.has_asr = checkboxGroup.includes('has_asr') ? 1 : 2
    // console.log(params)
    // 修改设备和添加设备请求后台
    const requestDevice = params.id ? editDevice : addDevice
    try {
      const result = await requestDevice(params)
      if (result.code === 0) message.success(result.desc)
      handleQueryList && (await handleQueryList())
      form.resetFields()
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <Modal
      title="请确认信息"
      visible={visible}
      width={800}
      onCancel={handleCancel}
      footer={null}
      forceRender
    >
      <Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
        {data.id && (
          <Form.Item name="id" rules={[{ required: true }]} style={{ display: 'none' }}>
            <Input hidden />
          </Form.Item>
        )}
        <Form.Item name="app_id" label="app_id" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name="name" label="设备名称" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name="comment" label="产品名称" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name="vendor" label="厂商" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item name="device_type_id" label="设备类型" rules={[{ required: true }]}>
          <Radio.Group>
            <Radio value={1}>soundbox</Radio>
            <Radio value={2}>phone</Radio>
            <Radio value={3}>TV</Radio>
            <Radio value={4}>IOT</Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item name="checkboxGroup" label="设备应用于" rules={[{ required: true }]}>
          <Checkbox.Group style={{ width: '100%' }}>
            <Row>
              <Col span={5}>
                <Checkbox value="has_wakeup" style={{ lineHeight: '32px' }}>
                  唤醒数据集
                </Checkbox>
              </Col>
              <Col span={5}>
                <Checkbox value="has_asr" style={{ lineHeight: '32px' }}>
                  ASR数据集
                </Checkbox>
              </Col>
            </Row>
          </Checkbox.Group>
        </Form.Item>
        <div className="modal-footer mt10">
          <Space>
            <Button type="primary" htmlType="submit">
              确认
            </Button>
            <Button htmlType="button" onClick={handleCancel}>
              取消
            </Button>
          </Space>
        </div>
      </Form>
    </Modal>
  )
}
