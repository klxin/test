import React, { useEffect, useState } from 'react'
import { PageHeaderWrapper } from '@ant-design/pro-layout'
import { Card, Button, Divider } from 'antd'
import { FileAddOutlined, EditOutlined } from '@ant-design/icons'
import { queryAllDevices } from '@/services'
import useTablePro, { TablePro } from '@/components/TablePro'
import { SUCCESS, ERROR } from '@/constants/global'
import { columns } from './columns'
import { DeviceModal } from './deviceModal'
import { cloneDeep } from '@/utils/tools'

export default function() {
  const [modalData, setModalData] = useState({})
  const [visible, setVisible] = useState(false)
  const { state, dispatch } = useTablePro()

  async function handleQueryList() {
    try {
      const res = await queryAllDevices()
      if (res.code !== 0) dispatch({ type: ERROR })
      dispatch({
        type: SUCCESS,
        payload: { data: res.data, pagination: { total: res.data.length } },
      })
      setVisible(false)
      setModalData({})
    } catch (error) {
      console.error(error)
      dispatch({ type: ERROR })
    }
  }
  useEffect(() => {
    queryAllDevices()
      .then(res => {
        if (res.code !== 0) dispatch({ type: ERROR })
        dispatch({
          type: SUCCESS,
          payload: { data: res.data, pagination: { total: res.data.length } },
        })
      })
      .catch(error => {
        console.error(error)
        dispatch({ type: ERROR })
      })
  }, [dispatch])

  const handleShowModal = () => setVisible(true)
  const closeModal = () => {
    setVisible(false)
    setModalData({})
  }
  const handleEditDevice = row => {
    const newRow = cloneDeep(row)
    // 处理数据
    const checkboxGroup = []
    newRow.has_wakeup === 1 && checkboxGroup.push('has_wakeup')
    newRow.has_asr === 1 && checkboxGroup.push('has_wakeup')
    newRow.checkboxGroup = checkboxGroup
    setModalData(newRow)
    handleShowModal()
  }
  const deviceColumns = [
    ...columns,
    {
      title: '操作',
      // dataIndex: 'id',
      key: 'id',
      render: (text, record) => (
        <Button type="primary" icon={<EditOutlined />} onClick={() => handleEditDevice(record)}>
          修改
        </Button>
      ),
    },
  ]
  return (
    <PageHeaderWrapper>
      <Card>
        {/* 添加设备按钮 */}
        <Button type="primary" icon={<FileAddOutlined />} onClick={handleShowModal}>
          添加新设备
        </Button>
        <Divider />
        {/* 表格组件 */}
        <TablePro
          columns={deviceColumns}
          dataSource={state.data}
          // showPagination={false}
          loading={state.loading}
        />
        {/* 修改和新增模态框 */}
        <DeviceModal
          data={{ ...modalData }}
          visible={visible}
          closeModal={closeModal}
          handleQueryList={handleQueryList}
        />
      </Card>
    </PageHeaderWrapper>
  )
}
