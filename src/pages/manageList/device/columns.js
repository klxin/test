export const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'appid',
    dataIndex: 'app_id',
    key: 'app_id',
  },
  {
    title: '设备名称',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: '厂商',
    dataIndex: 'vendor',
    key: 'vendor',
  },
  {
    title: '产品名称',
    dataIndex: 'comment',
    key: 'comment',
  },
  {
    title: '设备类型',
    dataIndex: 'device_type_name',
    key: 'device_type_name',
  },
]
