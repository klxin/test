import React from 'react'
import { Form } from 'antd'

export default props => {
  const { onFinish = () => {}, onAddCallback = () => {} } = props
  const [form] = Form.useForm()
  return (
    <Form layout="inline" form={form} onFinish={onFinish}>
      <Form.Item>
        <span className="btn-green" onClick={() => onAddCallback()}>
          添加数据集
        </span>
      </Form.Item>
    </Form>
  )
}
