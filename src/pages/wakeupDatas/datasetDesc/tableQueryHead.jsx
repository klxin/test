import React from 'react'
import { Form, Button, Select, Input } from 'antd'
// import { useGetDatasetStatusList, useGetDatasetTypeList } from '@/hooks/enum'
import { useGetDatasetStatusList } from '@/hooks/enum'

export default props => {
  const { onFinish = () => {} } = props
  const [form] = Form.useForm()
  const statusList = useGetDatasetStatusList()
  // const typeList = useGetDatasetTypeList()
  return (
    <Form layout="inline" form={form} onFinish={onFinish}>
      <Form.Item label="ID" name="label_id">
        <Input placeholder="请输入要搜索的ID" allowClear />
      </Form.Item>
      <Form.Item label="数据集状态" name="status">
        <Select placeholder="请选择数据集状态" allowClear>
          {statusList.map(item => (
            <Select.Option key={item.status_id} value={item.status_id}>
              {item.status_name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      {/* <Form.Item label="数据集类型" name="type_id">
        <Select placeholder="请选择数据集类型" allowClear>
          {typeList.map(item => (
            <Select.Option key={item.type_id} value={item.type_id}>
              {item.type_name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item> */}
      <Form.Item>
        <Button type="primary" htmlType="submit">
          查询
        </Button>
      </Form.Item>
    </Form>
  )
}
