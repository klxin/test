import React, { useRef, useState } from 'react'
import ProTable from '@ant-design/pro-table'
import { Space, Card } from 'antd'
import { DATASET_DESC_COLUMNS } from '@/constants/dataset'
import { filterNullParams } from '@/utils/tools'
import { statusMapBtn } from '@/components/Common'
import { typeIdMapApi } from './typeIdMapApi'
import { DescModal } from './descModal'
import TableQueryHead from './tableQueryHead'

export default (props) => {
  const { query } = props.location
  const actionRef = useRef()
  const [querys, setQuerys] = useState({
    data_set_id: +query.data_set_id,
    status: (query.status && +query.status) || null,
    label_id: 1,
    type_id: (query.type_id && +query.type_id) || null,
  })
  const [visible, setVisible] = useState(false)
  const [modalType, setModalType] = useState('label')
  const [params, setParams] = useState({ ...query })
  const handleResetModal = () => {
    setVisible(false)
    actionRef.current.reload()
  }
  const handleOk = () => {
    handleResetModal()
  }
  const handleCancel = () => {
    handleResetModal()
  }
  const handleLable = (id, modalTypeVal) => {
    setQuerys({ ...querys, label_id: id })
    setModalType(modalTypeVal)
    setVisible(true)
  }
  // table的请求函数
  const request = async (paramList = {}) => {
    const { current, ...param } = paramList
    const result = await typeIdMapApi(query.type_id).queryDatasetDesc({
      ...param,
      page: current,
    })
    const { labels } = result.data
    return Promise.resolve({ data: labels, success: true })
  }
  const onFinish = (values) => {
    console.log(params)
    const { label_id, status, ...payload } = params
    setParams({ ...payload, ...filterNullParams(values) })
  }
  const COLUMNS = [
    ...DATASET_DESC_COLUMNS,
    {
      title: '操作',
      dataIndex: '_',
      render: (_, { id, status }) => {
        const btnList = [
          <span className="btn-green" onClick={() => handleLable(id, 'label')}>
            标注
          </span>,
          <span className="btn-orange" onClick={() => handleLable(id, 'check')}>
            复核
          </span>,
        ]
        return <Space>{statusMapBtn(status, btnList)}</Space>
      },
    },
  ]
  return (
    <Card>
      <h1>红米note7自研线唤醒引擎_所有版本</h1>
      <TableQueryHead onFinish={onFinish} />
      <ProTable
        columns={COLUMNS}
        actionRef={actionRef}
        request={request}
        rowKey="id"
        search={false}
        params={params}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
      />
      {visible && (
        <DescModal
          query={querys}
          handleOk={handleOk}
          handleCancel={handleCancel}
          modalType={modalType}
        />
      )}
    </Card>
  )
}
