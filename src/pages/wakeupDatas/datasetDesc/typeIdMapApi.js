// 随机抽取&按设备抽取详情页的获取数据列表接口和asr中是同一个接口
import { queryDataSetDesc } from '@/services/asr'
import {
  setWakeupLabel,
  checkWakeupLabel,
  queryNextWakeupLabel,
  querySuspectDatasetDesc,
  setSuspectLabel,
  checkSuspectLabel,
  queryCheckSuspectLabel,
} from '@/services/dataset'

// 随机抽样数据集页面type_id就是1
// 按设备抽取type_id=2
// 疑似唤醒数据集type_id=3
const strategyMap = {
  1: {
    // 详情页的获取数据列表展示到表格的接口
    queryDatasetDesc: queryDataSetDesc,
    // 详情页 设置标注接口
    datasetSetLabel: setWakeupLabel,
    // 详情页 复核接口
    datasetCheckLabel: checkWakeupLabel,
    // 详情页 获取下页接口
    datasetNextLabel: queryNextWakeupLabel,
  },
  2: {
    queryDatasetDesc: queryDataSetDesc,
    datasetSetLabel: setWakeupLabel,
    datasetCheckLabel: checkWakeupLabel,
    datasetNextLabel: queryNextWakeupLabel,
  },
  3: {
    queryDatasetDesc: querySuspectDatasetDesc,
    datasetSetLabel: setSuspectLabel,
    datasetCheckLabel: checkSuspectLabel,
    datasetNextLabel: queryCheckSuspectLabel,
  },
}
export const typeIdMapApi = (type_id = 1) => {
  if (!type_id) return console.error('type_id为必传项')
  return strategyMap[type_id] && strategyMap[type_id]
}
