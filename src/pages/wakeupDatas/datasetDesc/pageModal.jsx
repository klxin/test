import React, { useEffect, useState, useRef } from 'react'
import { Input, Checkbox, Row, Col, Radio } from 'antd'
import { FilterProcess } from '@/components/Common'
import WaveSurfer from 'wavesurfer.js'

const options = [
  { label: 'TTS音', value: 'is_tts' },
  { label: '儿音', value: 'is_child' },
  { label: '背景噪声', value: 'is_noise' },
  { label: '闲聊', value: 'is_chat' },
]
const INIT_CHECKGROUP = {
  is_tts: false,
  is_noise: false,
  is_chat: false,
  is_child: false,
}
// 弹框 头部区域
export function ModalHeadArea(props) {
  const { dataDesc = {}, query = {} } = props
  useEffect(() => {
    let wavesurfer = WaveSurfer.create({
      container: '#waveform',
      waveColor: 'violet',
      progressColor: 'purple',
      height: 50,
    })
    wavesurfer.on('ready', () => {
      wavesurfer.play()
    })
    dataDesc.url && wavesurfer.load(dataDesc.url)
    return () => (wavesurfer = null)
  }, [dataDesc.url])
  return (
    <>
      <Row>
        <Col span={24}>
          <div id="waveform" />
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <audio src={dataDesc.url} controls />
        </Col>
      </Row>
      <Row>
        <Col span={5}>
          <strong>Id:</strong>
        </Col>
        <Col span={19}>{query.label_id}</Col>
      </Row>
      <Row>
        <Col span={5}>
          <strong>Progress:</strong>
        </Col>
        <Col span={19}>{FilterProcess(dataDesc.progress)}</Col>
      </Row>
      <Row>
        <Col span={5}>
          <strong>RequestId:</strong>
        </Col>
        <Col span={19}>{dataDesc.request_id}</Col>
      </Row>
      <Row>
        <Col span={5}>
          <strong>DeviceId:</strong>
        </Col>
        <Col span={19}>{dataDesc.device_id}</Col>
      </Row>
    </>
  )
}

// 弹框 主体区域
export function ModalContentArea(props) {
  const { onChange } = props
  const [labelValue, setLabelValue] = useState(1)
  const [inputValues, setInputValues] = useState({ asr: undefined, real_word: undefined })
  const [checkedValues, setCheckedValues] = useState({})
  // 以下主要解决不能及时获取到最新的数据
  const labelValueRef = useRef(labelValue)
  const inputValuesRef = useRef(inputValues)
  const checkedValuesRef = useRef(checkedValues)
  useEffect(() => {
    // 每次 更新 把值 复制给 labelValueRef
    labelValueRef.current = labelValue
  }, [labelValue])
  useEffect(() => {
    // 每次 更新 把值 复制给 inputValuesRef
    inputValuesRef.current = inputValues
  }, [inputValues])
  useEffect(() => {
    // 每次 更新 把值 复制给 checkedValuesRef
    checkedValuesRef.current = checkedValuesRef
  }, [checkedValuesRef])
  function onChangeRadio(e) {
    setLabelValue(e.target.value)
    // **设置一个延迟 0毫秒,这个 很重要**
    setTimeout(onChangeGetData, 0)
  }
  function onChangeCheckbox(values) {
    const obj = { ...INIT_CHECKGROUP }
    values.forEach((item) => {
      obj[item] = true
    })
    setCheckedValues(obj)
    // **设置一个延迟 0毫秒,这个 很重要**
    setTimeout(onChangeGetData, 0)
  }
  function onChangeInput(key, val) {
    setInputValues({ ...inputValues, [key]: val })
    // **设置一个延迟 0毫秒,这个 很重要**
    setTimeout(onChangeGetData, 0)
  }
  function onChangeGetData() {
    const params = { label: labelValueRef.current }
    if (labelValueRef.current === 2) {
      params.asr = inputValuesRef.current.asr
    } else if (labelValueRef.current === 4) {
      params.real_word = inputValuesRef.current.real_word
      return onChange({ ...params, ...checkedValues })
    }
    onChange(params)
  }
  return (
    <>
      <Row>
        <Col span={5}>请标注：</Col>
        <Col span={19}>
          <a href="https://wiki.n.miui.com/pages/viewpage.action?pageId=154446994">查看标注标准</a>
        </Col>
      </Row>
      <Radio.Group
        name="label"
        value={labelValue}
        style={{ width: '100%', fontSize: '14px' }}
        onChange={onChangeRadio}
      >
        <Row className="mt10">
          <Col span={5}>唤醒分类：</Col>
          <Col span={19}>
            <Radio value={1}>仅有唤醒词</Radio>
            <Radio value={2}>唤醒词+指令</Radio>
            <Input
              disabled={labelValue !== 2}
              onChange={(e) => onChangeInput('asr', e.target.value)}
              name="asr"
              value={inputValues.asr}
              placeholder="请输入实际的 唤醒词+指令"
            />
          </Col>
        </Row>
        <Row className="mt10">
          <Col span={5}>误唤醒分类：</Col>
          <Col span={19}>
            <Radio value={3}>无意中提到唤醒词</Radio>
            <Radio value={4}>没有唤醒词</Radio>
            <Input
              disabled={labelValue !== 4}
              onChange={(e) => onChangeInput('real_word', e.target.value)}
              name="real_word"
              value={inputValues.real_word}
              placeholder="请输入实际的音频内容"
            />
            <Row className="mt10">
              <Col span="5">具体分类 </Col>
              <Col span="19">
                <Checkbox.Group
                  options={options}
                  disabled={labelValue !== 4}
                  onChange={onChangeCheckbox}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="mt10">
          <Col span={5}>无意义分类：</Col>
          <Col span={19}>
            <Radio value={5}>听不清</Radio>
            <Radio value={6}>音质问题</Radio>
            <Radio value={7}>无意义</Radio>
          </Col>
        </Row>
      </Radio.Group>
    </>
  )
}
