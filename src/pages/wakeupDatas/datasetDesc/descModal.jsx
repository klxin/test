import React, { useState, useEffect } from 'react'
import { Modal, Divider, Space, message, Spin } from 'antd'
import { useQueryLabelInfoById } from '@/hooks/dataset'
import { filterNullParams } from '@/utils/tools'
import { typeIdMapApi } from './typeIdMapApi'
import { ModalHeadArea, ModalContentArea } from './pageModal'
import styles from './index.less'
// import localUrl from './20201122.wav'

export const DescModal = (props) => {
  const { query, handleOk, handleCancel, modalType = 'label' } = props
  const labelInfo = useQueryLabelInfoById(query)
  const [loading, setLoading] = useState(true)
  const [dataDesc, setDataDesc] = useState(labelInfo)
  useEffect(() => {
    setDataDesc(labelInfo)
  }, [labelInfo])
  // 请求数据对象
  const [data, setData] = useState({ label: 1 })
  const onChange = (values) => {
    setData(values)
  }
  const handleSubmit = async (nextStep = 'next') => {
    const { data_set_id, ...values } = query
    const requestParams = filterNullParams({ ...values, ...data })
    try {
      setLoading(false)
      const result =
        modalType === 'label'
          ? await typeIdMapApi(query.type_id).datasetSetLabel(requestParams)
          : await typeIdMapApi(query.type_id).datasetCheckLabel(requestParams)
      setLoading(true)
      if (result.code !== 0) return false
      message.success('提交成功')
      nextStep !== 'next' && handleOk && handleOk()
      return true
    } catch (error) {
      setLoading(true)
      console.error(error)
      return false
    }
  }
  const handleSubmitAndContinue = async () => {
    const isContinue = await handleSubmit('next')
    if (!isContinue) return
    const nextAsrQuery = { ...query, status: modalType === 'label' ? 0 : 3 }
    try {
      const { data: res, code } = await typeIdMapApi(query.type_id).datasetNextLabel(nextAsrQuery)
      setLoading(true)
      if (code !== 0) return
      setData({ ...data, label_id: res.id })
      if (Object.keys(res).length === 0) return handleOk && handleOk()
      setDataDesc(res)
    } catch (error) {
      setLoading(true)
      console.error(error)
    }
  }
  const footer = () => {
    return (
      <Space className="mt10">
        <span className="btn-primary" onClick={handleSubmit}>
          {modalType === 'label' ? '提交' : '复核'}
        </span>
        <span className="btn-green" onClick={handleSubmitAndContinue}>
          {modalType === 'label' ? '提交并继续标注' : '提交复核并继续标注'}
        </span>
      </Space>
    )
  }
  return (
    <Modal
      className={styles.descModal}
      title="请确认信息"
      visible
      onOk={handleOk}
      onCancel={handleCancel}
      width="60%"
      footer={footer()}
    >
      {/* 渲染头部信息区域 */}
      <ModalHeadArea dataDesc={dataDesc} query={query} />
      <Divider />
      {/* 渲染内容区域 */}
      <ModalContentArea onChange={onChange} />
      {loading ? null : (
        <div className="full-absolute">
          <Spin tip="Loading..." />
        </div>
      )}
    </Modal>
  )
}
