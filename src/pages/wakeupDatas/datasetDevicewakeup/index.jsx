import React from 'react'
import { Card, Divider } from 'antd'
import ContentPage from '@/components/DatasetContentPage/index'
import { ENUM_DATASET_PAGE } from '@/constants/dataset'
import TableQueryHead from './tableQueryHead'

export default () => {
  return (
    <Card>
      <h2>唤醒数据集-按设备抽取</h2>
      <h6>
        点击开始标注后才能进行标注，点击完成标注即可计算相关指标。抽取方式为：随机抽取 100
        个设备的全天唤醒数据；
      </h6>
      <Divider />
      {/* 主题展示区域 */}
      <ContentPage
        render={({ onFinish, onAddCallback }) => (
          <TableQueryHead onFinish={onFinish} onAddCallback={onAddCallback} />
        )}
        pageName={ENUM_DATASET_PAGE[1]}
      />
    </Card>
  )
}
