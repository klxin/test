import React from 'react'
import { Card, Divider } from 'antd'
import ContentPage from '@/components/DatasetContentPage/index'
import { queryDatasetList } from '@/services/dataset'
import TableQueryHead from './tableQueryHead'

export default () => {
  return (
    <Card>
      <h2>唤醒数据集-过ASR模型抽样</h2>
      <h6>点击开始标注后才能进行标注，点击完成标注即可计算相关指标。</h6>
      <h6>抽取方式为:随机抽取4000-5000条,过ASR模型后不包含"小爱同学"的数据入库标注</h6>
      <Divider />
      {/* 主题展示区域 */}
      <ContentPage
        requestApi={queryDatasetList}
        render={({ onFinish, onAddCallback }) => (
          <TableQueryHead onFinish={onFinish} onAddCallback={onAddCallback} />
        )}
      />
    </Card>
  )
}
