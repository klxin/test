import React, { useState } from 'react'
import { PageHeaderWrapper } from '@ant-design/pro-layout'
import { Dropdown, Button, Row, Col, Card, Steps } from 'antd'
import { DownOutlined, UserOutlined } from '@ant-design/icons'
import styles from './index.less'
import { useGetStatistics, useGetAllDevices, useGetVersions } from '../hooks/index'
import { DeviceList } from './deviceList'

export default () => {
  const initParams = {
    group: 0,
    device: 1,
    type: 1,
    version: 'ALL',
  }
  const [params, setParams] = useState(initParams)
  const statistics = useGetStatistics()
  const devices = useGetAllDevices()
  const versions = useGetVersions()

  function changeParams(key, val) {
    setParams({ ...params, [key]: val })
    console.log(params)
  }
  function handleMenuClick(e) {
    console.log('click', e)
    // TODO: 修改initParams.version
  }
  const menu = (
    <ul onClick={handleMenuClick}>
      {versions &&
        versions.map((item) => (
          <li key="1" icon={<UserOutlined />}>
            {item}
          </li>
        ))}
    </ul>
  )

  return (
    <PageHeaderWrapper>
      <div className="box">
        <div className={styles['box-title']}>
          {/* <p className="font-gray">
            设备：<span className="font-green">S12A音箱自研唤醒引擎</span>按
            <span className="font-orange">月</span>展示 版本：<span>ALL</span>
          </p> */}
          <div
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: statistics && statistics.brief && statistics.brief.title,
            }}
          />
          <Dropdown overlay={menu}>
            <Button type="primary">
              选择版本 <DownOutlined />
            </Button>
          </Dropdown>
        </div>
        <div className={styles['box-list']}>
          <Row gutter={16}>
            <Col span={8}>
              <Card size="small" title="统计周期">
                <DeviceList changeParams={changeParams} />
              </Card>
            </Col>
            <Col span={16}>
              <Card size="small" title="设备列表">
                <DeviceList type="device" list={devices} changeParams={changeParams} />
              </Card>
            </Col>
          </Row>
        </div>
        <div className={styles['box-desc']}>
          <Row gutter={16}>
            <Col span={8}>
              <Card size="small">
                <Steps
                  direction="vertical"
                  progressDot={<h1>dsfsdf</h1>}
                  current={params.group}
                  onChange={(current) => changeParams('group', current)}
                >
                  <Steps.Step title="监听" />
                  <Steps.Step title="监听-> 唤醒" />
                  <Steps.Step title="唤醒" />
                  <Steps.Step title="唤醒 -> 接收唤醒音频" />
                  <Steps.Step title="接收唤醒音频" />
                  <Steps.Step title="接收唤醒音频 -> 进行交互" />
                  <Steps.Step title="进行交互" />
                </Steps>
              </Card>
            </Col>
            <Col span={16}>
              <Card size="small" />
            </Col>
          </Row>
        </div>
      </div>
    </PageHeaderWrapper>
  )
}
