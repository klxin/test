import React from 'react'

const consultColor = {
  // 时间对应的样式
  day: 'purple',
  week: 'green',
  month: 'orange',
  // 设备类型对应的颜色
  soundbox: 'orange',
  phone: 'green',
  TV: 'purple',
  IOT: 'primary',
}
const initTimeList = [
  { id: 1, device_type_name: 'day', name: '日' },
  { id: 2, device_type_name: 'week', name: '周' },
  { id: 3, device_type_name: 'month', name: '月' },
]
export function DeviceList({ type = 'type', list = initTimeList, changeParams }) {
  return (
    <ul className={type === 'device' ? 'device-list' : 'time-list'}>
      {list.map(item => {
        const colorKey = item.device_type_name
        return (
          <li
            key={item.id}
            className={`bg-${consultColor[colorKey]}`}
            onClick={() => changeParams && changeParams(type, item.id)}
          >
            {item.name}
          </li>
        )
      })}
    </ul>
  )
}
