import React, { Input, Radio, Row, Col } from 'antd'

const options = [
  { label: '谐音', value: 29 },
  { label: '童音', value: 59 },
  { label: '英文', value: 26 },
  { label: '读音与文字不一致', value: 28 },
  { label: '口音', value: 58 },
  { label: '方言', value: 27 },
  { label: '非badcase', value: 43 },
  { label: '无效数据', value: 30 },
]
// 弹框 头部区域
export function ModalHeadArea(props) {
  const { dataDesc = {} } = props
  return (
    <ul>
      <Row>
        <Col span="24">
          <audio src={dataDesc.url} controls />
        </Col>
      </Row>
      <li />
      <Row>
        <Col span="5">
          <strong>ID：</strong>
        </Col>
        <Col span="19">{dataDesc.id}</Col>
      </Row>
      <Row>
        <Col span="5">
          <strong>Progress：</strong>
        </Col>
        <Col span="19">
          <span className="font-primary">{dataDesc.wait_check_count}</span>/
          <span className="font-orange">{dataDesc.has_check_count}</span>/
          <span className="font-gray">{dataDesc.total_count}</span>
        </Col>
      </Row>
      <Row>
        <Col span="5">
          <strong>RequestId：</strong>
        </Col>
        <Col span="19">{dataDesc.request_id}</Col>
      </Row>
    </ul>
  )
}

// 弹框 主体区域
export function ModalContentArea(props) {
  const { bad_case_label, handleInputChange } = props
  return (
    <ul>
      <Row>
        <Col span="5">
          <strong>请标注：</strong>
        </Col>
        <Col span="19">
          (<a href="https://wiki.n.miui.com/pages/viewpage.action?pageId=144466994">查看标注标准</a>
          )
        </Col>
      </Row>
      <Row className="mt10">
        <Col span="5">
          <strong>实际结果：</strong>
        </Col>
        <Col span="19">
          <Input
            name="label"
            style={{ width: 180 }}
            placeholder={bad_case_label}
            onChange={(e) => handleInputChange('label', e.target.value)}
          />
        </Col>
      </Row>
      <Row className="mt10">
        <Col span="5">
          <strong>分发原因：</strong>
        </Col>
        <Col span="19">
          {' '}
          <Radio.Group
            name="reason_id"
            onChange={(e) => handleInputChange('reason_id', e.target.value)}
            defaultValue={29}
          >
            {options.map((item) => (
              <Radio key={item.value} value={item.value}>
                {item.label}
              </Radio>
            ))}
          </Radio.Group>
        </Col>
      </Row>
    </ul>
  )
}
