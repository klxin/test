import React from 'react'
import { Form, Button, Select, DatePicker, Input } from 'antd'
import dayjs from 'dayjs'
import { useQueryBadcaseAppId, useQueryBadcaseUser } from '@/hooks/asr'
import { useGetBadcaseReasonList } from '@/hooks/enum'

const { RangePicker } = DatePicker
export default props => {
  const { onFinish = () => {} } = props
  const [form] = Form.useForm()
  // 获取所有的appid和复核人信息
  const appIds = useQueryBadcaseAppId()
  const users = useQueryBadcaseUser()
  const reasonList = useGetBadcaseReasonList()
  const onFinish1 = values => {
    const { request_time, update_time, ...params } = values
    onFinish &&
      onFinish({
        ...params,
        start_time: request_time && dayjs(request_time[0]).format('YYYY-MM-DD'),
        end_time: request_time && dayjs(request_time[1]).format('YYYY-MM-DD'),
        update_stime: update_time && dayjs(update_time[0]).format('YYYY-MM-DD'),
        update_etime: update_time && dayjs(update_time[1]).format('YYYY-MM-DD'),
      })
  }
  return (
    <Form layout="inline" form={form} onFinish={onFinish1}>
      <Form.Item label="appid" name="app_id">
        <Select placeholder="请选择设备" allowClear>
          {appIds &&
            appIds.map(item => (
              <Select.Option key={item} value={item}>
                {item}
              </Select.Option>
            ))}
        </Select>
      </Form.Item>
      <Form.Item label="分发原因" name="reason_id">
        <Select placeholder="请选择分发原因" allowClear>
          {reasonList &&
            reasonList.map(item => (
              <Select.Option key={item.reason_id} value={item.reason_id}>
                {item.reason_name}
              </Select.Option>
            ))}
        </Select>
      </Form.Item>
      <Form.Item label="query" name="query">
        <Input placeholder="请输入要查询的query" />
      </Form.Item>
      <Form.Item label="复核人" name="user_name">
        <Select placeholder="请选择复核人" allowClear>
          {users &&
            users.map(item => (
              <Select.Option key={item} value={item}>
                {item}
              </Select.Option>
            ))}
        </Select>
      </Form.Item>
      <Form.Item label="请求日期" name="request_time">
        <RangePicker format="YYYY-MM-DD" />
      </Form.Item>
      <Form.Item label="更新日期" name="update_time">
        <RangePicker format="YYYY-MM-DD" />
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          查询
        </Button>
      </Form.Item>
    </Form>
  )
}
