import React, { useState, useEffect } from 'react'
import { Modal, Divider, Space, message } from 'antd'
import { useQueryBadcaseById } from '@/hooks/asr'
import { setBadcaseLabel, queryNextBadcaseLabel } from '@/services/asr'
import { ModalHeadArea, ModalContentArea } from './pageModal'
import styles from './index.less'

export const BadCaseModal = (props) => {
  const { query, handleOk, handleCancel } = props
  const labelInfo = useQueryBadcaseById(query.label_id)
  const [dataDesc, setDataDesc] = useState({})
  useEffect(() => {
    setDataDesc({ ...labelInfo })
  }, [labelInfo])
  // 请求数据对象
  const [data, setData] = useState({ reason_id: 29, label: '', label_id: query.label_id })
  const handleInputChange = (key, val) => {
    setData({ ...data, [key]: val })
  }
  const handleSubmit = async (nextStep = 'next') => {
    const requestParams = { ...query, ...data }
    try {
      const result = await setBadcaseLabel(requestParams)
      if (result.code !== 0) return false
      message.success('提交成功')
      nextStep !== 'next' && handleOk && handleOk()
      return true
    } catch (error) {
      console.error(error)
      return false
    }
  }
  const handleSubmitAndContinue = async () => {
    if (!handleSubmit('next')) return
    try {
      const { data, code } = await queryNextBadcaseLabel()
      if (code !== 0) return
      setData({ ...data, label_id: data.id })
      if (Object.keys(data).length !== 0) return setDataDesc(data)
      message.success('提交完毕...')
      handleOk && handleOk()
    } catch (error) {
      console.error(error)
    }
  }
  const footer = () => {
    return (
      <Space className="mt10">
        <span className="btn-primary" onClick={handleSubmit}>
          提交
        </span>
        <span className="btn-green" onClick={handleSubmitAndContinue}>
          提交并继续标注
        </span>
      </Space>
    )
  }
  return (
    <Modal
      className={styles.badCaseModal}
      title="请确认信息"
      visible
      onOk={handleOk}
      onCancel={handleCancel}
      width="60%"
      footer={footer()}
    >
      {/* 渲染头部信息区域 */}
      <ModalHeadArea dataDesc={dataDesc} query={query} />
      <Divider />
      {/* 渲染内容区域 */}
      <ModalContentArea
        bad_case_label={dataDesc.bad_case_label}
        handleInputChange={handleInputChange}
      />
    </Modal>
  )
}
