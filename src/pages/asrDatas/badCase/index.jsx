import React, { useRef, useState } from 'react'
import { Card, Space, Divider, Radio } from 'antd'
import ProTable from '@ant-design/pro-table'
import { queryBadcases as queryBalases } from '@/services/asr'
import { ASR_BADCASE_COLUMNS } from '@/constants/asr'
import { filterNullParams } from '@/utils/tools'
import TableQueryHead from './tableQueryHead'
import { BadCaseModal } from './badCaseModal'

export default () => {
  const actionRef = useRef()
  const [params, setParams] = useState({})
  const [status, setStatus] = useState('0')
  const [labelId, setLabelId] = useState(0)
  const [visible, setVisible] = useState(false)
  const handleResetModal = () => {
    setVisible(false)
  }
  const handleOk = () => {
    handleResetModal()
  }
  const handleCancel = () => {
    handleResetModal()
  }
  const onFinish = (values) => {
    // 设置params数据 重新请求表格数据
    setParams({ ...params, ...filterNullParams(values) })
  }
  const handleModeChange = (e) => {
    setStatus(e.target.value)
    setParams({ status: e.target.value })
  }
  const COLUMNS = [
    ...ASR_BADCASE_COLUMNS,
    {
      title: '操作',
      dataIndex: '_',
      render: (_, { id }) => (
        <span
          className="btn-primary"
          onClick={() => {
            setLabelId(id)
            setVisible(true)
          }}
        >
          标注
        </span>
      ),
    },
  ]
  return (
    <Card>
      <Radio.Group onChange={handleModeChange} value={status} style={{ marginBottom: 8 }}>
        <Space>
          <Radio.Button value="0">待复核</Radio.Button>
          <Radio.Button value="3">已复核</Radio.Button>
        </Space>
      </Radio.Group>

      <Divider />
      {/* 表格查询区域 */}
      <TableQueryHead onFinish={onFinish} />
      {/* 表格组件 */}
      <ProTable
        columns={COLUMNS}
        actionRef={actionRef}
        request={async (params = {}) => {
          const { current, ...requestQuery } = params
          return await queryBalases({ ...requestQuery, page: current })
          // return Promise.resolve({
          //   data: mockData,
          //   success: true,
          // })
        }}
        rowKey="id"
        search={false}
        params={params}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
      />
      {/* 弹框 */}
      {visible && (
        <BadCaseModal
          handleOk={handleOk}
          handleCancel={handleCancel}
          query={{ label_id: labelId }}
        />
      )}
    </Card>
  )
}
