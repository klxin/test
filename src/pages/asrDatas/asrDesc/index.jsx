import React, { useRef, useState } from 'react'
import ProTable from '@ant-design/pro-table'
import { Space } from 'antd'
import { ASR_DESC_COLUMNS } from '@/constants/asr'
import { queryDataSetDesc } from '@/services/asr'
import { DescModal } from './descModal'

export default function(props) {
  const { query } = props.location
  const actionRef = useRef()
  const [label_id, setLabelId] = useState(1)
  const [visible, setVisible] = useState(false)
  const [modalType, setModalType] = useState('label')
  const [params, setParams] = useState({ ...query })
  const handleResetModal = () => {
    setVisible(false)
    actionRef.current.reload()
  }
  const handleOk = () => {
    handleResetModal()
  }
  const handleCancel = () => {
    handleResetModal()
  }
  const handleLable = (id, modalType) => {
    setLabelId(id)
    setModalType(modalType)
    setVisible(true)
  }
  // table的请求函数
  const request = async (params = {}) => {
    const { current, ...param } = params
    const result = await queryDataSetDesc({
      ...param,
      page: current,
    })
    const { labels } = result.data
    return Promise.resolve({ data: labels, success: true })
  }
  const toolbar = {
    search: {
      placeholder: '请输入要查询的ID',
      onSearch: value => {
        // alert(value)
        setParams({ ...params, label_id: value })
      },
    },
  }
  const COLUMNS = [
    ...ASR_DESC_COLUMNS,
    {
      title: '操作',
      dataIndex: 'id',
      render: id => {
        return (
          <Space>
            <span className="btn-green" onClick={() => handleLable(id, 'label')}>
              标注
            </span>
            <span className="btn-orange" onClick={() => handleLable(id, 'check')}>
              复核
            </span>
          </Space>
        )
      },
    },
  ]
  return (
    <>
      <h1>ASR+NLP语音助手</h1>
      <ProTable
        columns={COLUMNS}
        actionRef={actionRef}
        request={request}
        toolbar={toolbar}
        rowKey="id"
        search={false}
        params={params}
        pagination={{
          pageSize: 10,
        }}
        dateFormatter="string"
      />
      {visible && (
        <DescModal
          query={{ data_set_id: +query.data_set_id, label_id: +label_id }}
          handleOk={handleOk}
          handleCancel={handleCancel}
          modalType={modalType}
        />
      )}
    </>
  )
}
