import React, { useEffect, useState } from 'react'
import { Input, Checkbox, Row, Col } from 'antd'
import { FilterProcess } from '@/components/Common'
import WaveSurfer from 'wavesurfer.js'
import { checkDomain } from '@/services/asr'

const options1 = [
  { label: '截断', value: 'is_cut' },
  { label: '噪音', value: 'is_noise' },
  { label: '电子音', value: 'is_tts' },
  { label: '多人说话', value: 'is_multi_sound' },
  { label: '发音模糊', value: 'is_unpredictable' },
  { label: '语义不明', value: 'is_nonsense' },
  { label: '其他原因', value: 'is_other' },
  { label: '无效数据', value: 'invalid' },
]
const options2 = [
  { label: '口音', value: 'is_dialect' },
  { label: '中英混合', value: 'is_mix' },
  { label: '儿童发音', value: 'is_child' },
]
// 弹框 头部区域
export function ModalHeadArea(props) {
  const { dataDesc = {}, query = {} } = props
  useEffect(() => {
    let wavesurfer = WaveSurfer.create({
      container: '#waveform',
      waveColor: 'violet',
      progressColor: 'purple',
      height: 50,
    })
    wavesurfer.on('ready', () => {
      wavesurfer.play()
    })
    dataDesc.url && wavesurfer.load(dataDesc.url)
    return () => (wavesurfer = null)
  }, [dataDesc.url, query])

  const [domain, setDomain] = useState('')
  useEffect(() => {
    checkDomain({ ...query, query: dataDesc.show_result })
      .then((res) => {
        const { domain: domainVal = '' } = res.data || {}
        domainVal && setDomain(domainVal)
      })
      .catch((err) => console.error(err))
  }, [dataDesc.show_result, query])
  return (
    <>
      <Row>
        <Col span={24}>
          <div id="waveform" />
        </Col>
      </Row>
      <Row>
        <Col span={24}>
          <audio src={dataDesc.url} controls />
        </Col>
      </Row>
      <Row>
        <Col span={5}>
          <strong>Id:</strong>
        </Col>
        <Col span={19}>{query.label_id}</Col>
      </Row>
      <Row>
        <Col span={5}>
          <strong>Progress:</strong>
        </Col>
        <Col span={19}>
          {FilterProcess(dataDesc.progress)}
          <span className="font-primary">已标注{dataDesc.user_label}条</span>
          <span className="font-primary">已复核{dataDesc.user_check}条</span>
        </Col>
      </Row>
      <Row>
        <Col span={5}>
          <strong>RequestId:</strong>
        </Col>
        <Col span={19}>{dataDesc.request_id}</Col>
      </Row>
      <Row>
        <Col span={5}>
          <strong>DeviceId:</strong>
        </Col>
        <Col span={19}>{dataDesc.device_id}</Col>
      </Row>
      <Row>
        <Col span={5}>
          <strong>Domain:</strong>
        </Col>
        <Col span={19}>{domain}</Col>
      </Row>
      <Row>
        <Col span={5}>
          <strong>Label:</strong>
        </Col>
        <Col span={19}>{dataDesc.label}</Col>
      </Row>
    </>
  )
}

// 弹框 主体区域
export function ModalContentArea(props) {
  const { show_result, handleInputChange, onChange } = props
  return (
    <>
      <Row>
        <Col span={5}>
          <strong>请标注：</strong>
        </Col>
        <Col span={19}>
          <a href="https://wiki.n.miui.com/pages/viewpage.action?pageId=144466994">查看标注标准</a>)
        </Col>
      </Row>
      <Row className="mt10">
        <Col span={5}>
          <strong>实际结果：</strong>
        </Col>
        <Col span={19}>
          <Input
            name="label"
            style={{ width: 180 }}
            placeholder={show_result}
            onChange={(e) => handleInputChange('label', e.target.value)}
          />
        </Col>
      </Row>
      <Row className="mt10">
        <Col span={5}>
          <strong>下线分类：</strong>
        </Col>
        <Col span={19}>
          <Checkbox.Group options={options1} onChange={onChange} />
        </Col>
      </Row>
      <Row className="mt10">
        <Col span={5}>
          <strong>其他标注：</strong>
        </Col>
        <Col span={19}>
          <Checkbox.Group options={options2} onChange={(values) => onChange(values, 'other')} />
          <Input
            placeholder="请输入其他原因"
            name="custom_reason"
            style={{ width: 80 }}
            onChange={(e) => handleInputChange('custom_reason', e.target.value)}
          />
        </Col>
      </Row>
    </>
  )
}
