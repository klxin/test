import React, { useState, useEffect } from 'react'
import { Modal, Divider, Space, message, Spin } from 'antd'
import { useQueryLabelInfo } from '@/hooks/asr'
import { setAsrLabel, queryNextAsrLabel, checkAsrLabel } from '@/services/asr'
import { ModalHeadArea, ModalContentArea } from './pageModal'
import styles from './index.less'
// import localUrl from './20201122.wav'

const INIT_DATA = {
  is_cut: false,
  is_noise: false,
  is_tts: false,
  is_multi_sound: false,
  is_unpredictable: false,
  is_nonsense: false,
  is_other: false,
  invalid: false,
  is_dialect: false,
  is_mix: false,
  is_child: false,
}
function getSelectData(values, type = 'offline') {
  const initList = {
    offline: [
      'is_cut',
      'is_noise',
      'is_tts',
      'is_multi_sound',
      'is_unpredictable',
      'is_nonsense',
      'is_other',
      'invalid',
    ],
    other: ['is_dialect', 'is_mix', 'is_child'],
  }
  const obj = { ...INIT_DATA }
  initList[type].forEach((item) => {
    if (values.includes(item)) {
      obj[item] = true
    } else {
      obj[item] = false
    }
  })
  return obj
}
export const DescModal = (props) => {
  const { query, handleOk, handleCancel, modalType = 'label' } = props
  const labelInfo = useQueryLabelInfo(query.label_id)
  const [loading, setLoading] = useState(true)
  const [dataDesc, setDataDesc] = useState(labelInfo)
  useEffect(() => {
    setDataDesc(labelInfo)
  }, [labelInfo])
  // 请求数据对象
  const [data, setData] = useState(INIT_DATA)
  const onChange = (values, type = 'offline') => {
    setData({ ...data, ...getSelectData(values, type) })
  }
  const handleInputChange = (key, val) => {
    setData({ ...data, [key]: val })
  }
  const handleSubmit = async (nextStep = 'next') => {
    const requestParams = { ...query, ...data }
    try {
      setLoading(false)
      const result =
        modalType === 'label'
          ? await setAsrLabel(requestParams)
          : await checkAsrLabel(requestParams)
      setLoading(true)
      if (result.code !== 0) return false
      message.success('提交成功')
      nextStep !== 'next' && handleOk && handleOk()
      return true
    } catch (error) {
      setLoading(true)
      console.error(error)
      return false
    }
  }
  const handleSubmitAndContinue = async () => {
    const isContinue = await handleSubmit('next')
    if (!isContinue) return
    const nextAsrQuery = { ...query, status: modalType === 'label' ? 0 : 3 }
    try {
      const { data: res, code } = await queryNextAsrLabel(nextAsrQuery)
      setLoading(true)
      if (code !== 0) return
      setData({ ...data, label_id: res.id })
      if (Object.keys(res).length === 0) return handleOk && handleOk()
      setDataDesc(res)
    } catch (error) {
      setLoading(true)
      console.error(error)
    }
  }
  const footer = () => {
    return (
      <Space className="mt10">
        <span className="btn-primary" onClick={handleSubmit}>
          {modalType === 'label' ? '提交' : '复核'}
        </span>
        <span className="btn-green" onClick={handleSubmitAndContinue}>
          {modalType === 'label' ? '提交并继续标注' : '提交复核并继续标注'}
        </span>
      </Space>
    )
  }
  return (
    <Modal
      className={styles.descModal}
      title="请确认信息"
      visible
      onOk={handleOk}
      onCancel={handleCancel}
      width="60%"
      footer={footer()}
    >
      {/* 渲染头部信息区域 */}
      <ModalHeadArea dataDesc={dataDesc} query={query} />
      <Divider />
      {/* 渲染内容区域 */}
      <ModalContentArea
        show_result={dataDesc.show_result}
        handleInputChange={handleInputChange}
        onChange={onChange}
      />
      {loading ? null : (
        <div className="full-absolute">
          <Spin tip="Loading..." />
        </div>
      )}
    </Modal>
  )
}
