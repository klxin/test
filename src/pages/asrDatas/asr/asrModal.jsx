import React from 'react'
import { ENUM_OPERATES } from '@/constants/asr'
import { ENUM_STATUS } from '@/constants/global'
import { DispatchModal, ReceiveModal, StatusModal, AddModal } from './pageModal'

const { LABELING, COMPLETE, INVALID } = ENUM_STATUS
/**
 *
 * @param {object: {id, type, handleOk, handleCancel }} props
 * id: {number} 数据集id
 * type: {string} 表示弹框类型
 * handleOk: 确认弹框的回调函数
 * handleCancel: 取消弹框的回调函数
 */
export const AsrModal = props => {
  const { id, type = ENUM_OPERATES[0], handleOk = () => {}, handleCancel = () => {} } = props
  const propsParams = {
    id,
    handleOk,
    handleCancel,
  }
  const strategyModalMap = {
    [ENUM_OPERATES[0]]: <DispatchModal {...propsParams} />, // 派发
    [ENUM_OPERATES[1]]: <ReceiveModal {...propsParams} />, // 领取
    [ENUM_OPERATES[2]]: <StatusModal {...propsParams} type_id={INVALID} />, // 下线
    [ENUM_OPERATES[3]]: <StatusModal {...propsParams} type_id={LABELING} />, // 上线
    [ENUM_OPERATES[4]]: <StatusModal {...propsParams} type_id={COMPLETE} />, // 完成
    [ENUM_OPERATES[5]]: <AddModal {...propsParams} />, // 添加数据集
  }
  return strategyModalMap[type] ? strategyModalMap[type] : null
}
