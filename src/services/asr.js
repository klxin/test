import request from '@/utils/request'

// 查询ASR数据集 列表数据
export async function queryAsrData(params) {
  return request('/template/asr_data_set', { params })
}
// 获取分派弹框-信息
export async function queryAssignInfo(params) {
  return request('/data/assign_info', { params })
}
// 确认分派弹框和领取弹框
export async function assignLabel(data) {
  return request('/data/assign_label', { method: 'POST', data })
}
// 获取领取弹框-信息
export async function queryLabelReceive(params) {
  return request('/data/load_label_receive', { params })
}
// 添加ASR弹框
export async function addAsrDataSet(data) {
  return request('/data/add_asr_data_set', { method: 'POST', data })
}
// 获取ASR-desc数据详情
export async function queryDataSetDesc(params) {
  return request('/template/data_set_desc', { params })
}
// 获取指定标注详情
export async function queryLabelInfo(params) {
  return request('/data/label_info', { params })
}
// 标注ASR数据
export async function setAsrLabel(data) {
  return request('/data/set_asr_label', { method: 'POST', data })
}
// 获取下一页标注信息
export async function queryNextAsrLabel(data) {
  return request('/data/next_asr_label', { method: 'POST', data })
}
// 复核ASR数据
export async function checkAsrLabel(data) {
  return request('/data/check_asr_label', { method: 'POST', data })
}
// 查询指定query所属的domain
export async function checkDomain(params) {
  return request('/data/check_domain', { params })
}
// 查询指定query所属的domain
export async function queryBadcases(params) {
  return request('/template/badcase', { params })
}

// Bad-Case
// 查询指定query所属的domain
export async function queryBadcaseAppId(params) {
  return request('/data/badcase_appid', { params })
}
// 查询指定query所属的domain
export async function queryBadcaseUser(params) {
  return request('/data/badcase_user', { params })
}
// 查询指定query所属的domain
export async function queryBadcaseById(params) {
  return request('/data/get_badcase', { params })
}
// 标注badcase数据
export async function setBadcaseLabel(data) {
  return request('/data/set_badcase_label', { method: 'POST', data })
}
// 获取下一条badcase数据
export async function queryNextBadcaseLabel() {
  return request('/data/next_badcase_label')
}
