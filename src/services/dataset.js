import request from '@/utils/request'

// 获取随机抽样页面数据
export async function queryDatasetList(params) {
  return request('/template/data_set', { params })
}
// 添加数据集
export async function addDataset(data) {
  return request('/data/add_data_set', { method: 'POST', data })
}
// 批量添加数据
export async function batchAddLabels(data) {
  return request('/data/batch_add_labels', { method: 'POST', data })
}
// 更新数据集状态
export async function setDatasetStatus(data) {
  return request('/data/set_data_set_status', { method: 'POST', data })
}
// 发送复核邮件
export async function sendCheckMail(data) {
  return request('/data/send_check_mail', { method: 'POST', data })
}
// 获取数据集指标
export async function queryDatasetResult(params) {
  return request('/data/data_set_result', { params })
}
// 批量添加数据
export async function downloadDataset(params) {
  return request('/data/download_data_set', { params, responseType: 'blob' })
}
// 分派数据集用户
export async function assignDatasetUser(data) {
  return request('/data/assign_data_set_user', { method: 'POST', data })
}
// 分派数据集用户
export async function batchAddDeviceLabels(data) {
  return request('/data/batch_add_device_labels', { method: 'POST', data })
}
// 获取数据集指标
export async function queryDataSetSuspectResult(params) {
  return request('/data/data_set_suspect_result', { params })
}

// 数据集详情页
// 获取指定id的标注数据信息
export async function queryLabelInfoById(data) {
  return request('/data/label_info_by_id', { method: 'POST', data })
}
// 标注数据
export async function setWakeupLabel(data) {
  return request('/data/set_wakeup_label', { method: 'POST', data })
}
// 复核数据
export async function checkWakeupLabel(data) {
  return request('/data/check_wakeup_label', { method: 'POST', data })
}
// 获取下一条标注数据
export async function queryNextWakeupLabel(data) {
  return request('/data/next_wakeup_label', { method: 'POST', data })
}

// 疑似唤醒数据集详情
// 获取疑似唤醒数据集列表
export async function querySuspectDatasetDesc(params) {
  return request('/template/suspect_data_set_desc', { params })
}
// 疑似唤醒数据集 标注数据
export async function setSuspectLabel(data) {
  return request('/data/set_suspect_label', { method: 'POST', data })
}
// 疑似唤醒数据集 复核数据
export async function checkSuspectLabel(data) {
  return request('/data/check_suspect_label', { method: 'POST', data })
}
// 疑似唤醒数据集 获取下一条标注数据
export async function queryCheckSuspectLabel(data) {
  return request('/data/next_suspect_label', { method: 'POST', data })
}
