import request from '@/utils/request'

// 添加设备
export async function addDevice(data) {
  return request('/data/add_device', { method: 'POST', data })
}
// 修改设备
export async function editDevice(data) {
  return request('/data/modify_device', { method: 'POST', data })
}
// 获取设备信息
export async function getDeviceInfo(params) {
  return request('/data/device_info', { params })
}
