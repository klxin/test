import request from '@/utils/request'

// 查询ASR数据集 列表数据
export async function queryMyLabel(params) {
  return request('/template/my_label', { params })
}
