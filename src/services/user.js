import request from '@/utils/request'

// 获取用户信息
export async function queryCurrent() {
  return request('/data/user_id')
}
// 获取用户列表数据
export async function queryUserList() {
  return request('/data/load_users')
}
// 获取分派弹框-人员列表
export async function queryLabelUser() {
  return request('/data/load_label_user')
}
