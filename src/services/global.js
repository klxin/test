import request from '@/utils/request'

// 设置数据集状态
export async function setStatus(data) {
  return request('/data/set_data_set_status', { method: 'POST', data })
}
