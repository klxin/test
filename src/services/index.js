import request from '@/utils/request'

// 获取统计数据
export async function queryStatistics() {
  return request('/statistics/option')
}
// 获取所有设备列表
export async function queryAllDevices() {
  return request('/data/get_device')
}
// 获取版本列表
export async function queryVersions() {
  return request('/statistics/version_list')
}
