import request from '@/utils/request'

// 获取数据集类型列表
export async function queryDatasetTypeList() {
  return request('/data/dataset_type_list')
}
// 获取数据集状态列表
export async function queryDatasetStatusList() {
  return request('/data/dataset_status_list')
}
// 获取badcase原因列表
export async function queryBadcaseReasonList() {
  return request('/data/badcase_reason_list')
}
