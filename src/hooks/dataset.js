import { useEffect, useState } from 'react'
import {
  queryDatasetResult,
  queryDataSetSuspectResult,
  queryLabelInfoById,
} from '@/services/dataset'
import { filterNullParams } from '@/utils/tools'
/**
 * @desc: 获取数据集指标
 * @return 返回查询的数据对象
 */
export const useGetDatasetResult = data_set_id => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryDatasetResult({ data_set_id })
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [data_set_id])
  return data
}
/**
 * @desc: 获取数据集指标
 * @return 返回查询的数据对象
 */
export const useGetDataSetSuspectResult = data_set_id => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryDataSetSuspectResult({ data_set_id })
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [data_set_id])
  return data
}
/**
 * @desc: 获取领取弹框的信息
 * @param {number} params {id: }查询条件对象
 * @return 返回查询的数据对象
 */
export const useQueryLabelInfoById = params => {
  const [data, setData] = useState({})
  useEffect(() => {
    queryLabelInfoById(filterNullParams(params))
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [params])
  return data
}
