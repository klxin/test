import { useEffect, useState } from 'react'
import {
  queryAssignInfo,
  queryLabelReceive,
  queryLabelInfo,
  queryBadcaseUser,
  queryBadcaseAppId,
  queryBadcaseById,
} from '../services/asr'

/**
 * @desc: 获取分派弹框的可设置信息
 * @param {number} params {id: }查询条件对象
 * @return 返回查询的数据对象
 */
export const useQueryAssignInfo = id => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryAssignInfo({ id })
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [id])
  return data
}
/**
 * @desc: 获取领取弹框的信息
 * @param {number} params {id: }查询条件对象
 * @return 返回查询的数据对象
 */
export const useQueryLabelReceive = id => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryLabelReceive({ id })
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [id])
  return data
}
/**
 * @desc: 获取领取弹框的信息
 * @param {number} params {id: }查询条件对象
 * @return 返回查询的数据对象
 */
export const useQueryLabelInfo = id => {
  const [data, setData] = useState({})
  useEffect(() => {
    queryLabelInfo({ id })
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [id])
  return data
}

/**
 * @desc: 获取badcase数据库中的全部appid
 * @return 返回查询的数据对象
 */
export const useQueryBadcaseUser = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryBadcaseUser()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [])
  return data
}
/**
 * @desc: 获取全部进行过badcase标注的人员列表
 * @return 返回查询的数据对象
 */
export const useQueryBadcaseAppId = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryBadcaseAppId()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [])
  return data
}
/**
 * @desc: 获取全部进行过badcase标注的人员列表
 * @return 返回查询的数据对象
 */
export const useQueryBadcaseById = label_id => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryBadcaseById({ label_id })
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [label_id])
  return data
}
