import { useEffect, useState } from 'react'
import {
  queryDatasetTypeList,
  queryDatasetStatusList,
  queryBadcaseReasonList,
} from '@/services/enum'

/**
 * @desc: 获取数据集类型列表
 * @return 返回查询的数据对象
 */
export const useGetDatasetTypeList = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryDatasetTypeList()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [])
  return data
}

/**
 * @desc: 获取数据集状态列表
 * @return 返回查询的数据对象
 */
export const useGetDatasetStatusList = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryDatasetStatusList()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [])
  return data
}

/**
 * @desc: 获取badcase原因列表
 * @return 返回查询的数据对象
 */
export const useGetBadcaseReasonList = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryBadcaseReasonList()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [])
  return data
}
