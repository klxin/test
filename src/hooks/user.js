import { useEffect, useState } from 'react'
import { queryCurrent, queryLabelUser } from '../services/user'

/**
 * @desc: 获取唤醒漏洞数据
 * @param {object} params 查询条件对象
 * @return 返回查询的数据对象
 */
export const useGetUserInfo = _ => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryCurrent()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [])
  return data
}
/**
 * @desc: 获取分派弹框的用户列表
 * @param {object} params 查询条件对象
 * @return 返回查询的数据对象
 */
export const useQueryLabelUsers = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryLabelUser()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [])
  return data
}
