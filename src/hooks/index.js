import { useEffect, useState } from 'react'
import { queryStatistics, queryAllDevices, queryVersions } from '../services/index'

/**
 * @desc: 获取唤醒漏洞数据
 * @param {object} params 查询条件对象
 * @return 返回查询的数据对象
 */
export const useGetStatistics = params => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryStatistics()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [params])
  return data
}

/**
 * @desc: 获取所有设备列表
 * @param {object} params 查询条件对象
 * @return 返回查询的数据对象
 */
export const useGetAllDevices = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryAllDevices()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [])
  return data
}

/**
 * @desc: 获取版本列表
 * @param {object} params 查询条件对象
 * @return 返回查询的数据对象
 */
export const useGetVersions = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    queryVersions()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [])
  return data
}
