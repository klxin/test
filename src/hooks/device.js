import { useEffect, useState } from 'react'
import { getDeviceInfo } from '../services/device'

/**
 * @desc: 获取设备信息
 * @return 返回查询的数据对象
 */
export const useDeviceInfo = params => {
  const [data, setData] = useState([])
  useEffect(() => {
    getDeviceInfo()
      .then(res => {
        setData(res.data)
      })
      .catch(err => console.error(err))
  }, [params])
  return data
}
