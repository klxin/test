export default [
  {
    path: '/',
    component: '../layouts/index',
    routes: [
      {
        path: '/',
        name: '唤醒数据漏斗',
        icon: 'dashboard',
        component: './index',
      },
      {
        path: '/my-label',
        name: '我的标注',
        icon: 'bars',
        component: './myLabel/index',
      },
      {
        path: '/wakeup-datas',
        name: '唤醒数据集',
        icon: 'folder-open',
        routes: [
          {
            path: '/wakeup-datas/data-set',
            name: '随机抽样',
            icon: 'file',
            component: './wakeupDatas/dataset/index',
          },
          {
            path: '/wakeup-datas/data-set-desc',
            // name: '唤醒数据详情',
            icon: 'file',
            component: './wakeupDatas/datasetDesc/index',
          },
          {
            path: '/wakeup-datas/data-set-model-filter',
            name: '随机抽样(ASR模型过滤)',
            icon: 'file',
            component: './wakeupDatas/datasetModelFilter/index',
          },
          {
            path: '/wakeup-datas/data-set-device-wakeup',
            name: '按设备抽取',
            icon: 'file',
            component: './wakeupDatas/datasetDeviceWakeup/index',
          },
          {
            path: '/wakeup-datas/data-set-suspect',
            name: '疑似唤醒随机抽样',
            icon: 'file',
            component: './wakeupDatas/datasetSuspect/index',
          },
          {
            path: '/wakeup-datas/data-set-suspect-desc',
            // name: '疑似唤醒随机详情',
            icon: 'file',
            component: './wakeupDatas/datasetSuspectDesc/index',
          },
        ],
      },
      {
        path: '/asr-datas',
        name: 'ASR数据集',
        icon: 'appstore',
        routes: [
          {
            path: '/asr-datas/asr',
            name: 'ASR数据集',
            icon: 'file',
            component: './asrDatas/asr/index',
          },
          {
            path: '/asr-datas/asr-desc',
            // name: 'ASR数据集详情',
            icon: 'file',
            component: './asrDatas/asrDesc/index',
          },
          {
            path: '/asr-datas/badcase',
            name: 'BadCase数据集',
            icon: 'file',
            component: './asrDatas/badCase/index',
          },
        ],
      },
      {
        path: '/manage-list',
        name: '工具箱',
        icon: 'tool',
        routes: [
          {
            path: '/manage-list/user',
            name: '用户管理',
            icon: 'file',
            component: './manageList/user/index',
          },
          {
            path: '/manage-list/device',
            name: '设备管理',
            icon: 'file',
            component: './manageList/device/index',
          },
        ],
      },
    ],
  },
  {
    component: './404',
  },
]
